EPAM Training Final Project.

Bicycle Rent Contour.

Project represents web-service for bicycle renting.
A user shall pass registration process and get access to renting service.
Before making any orders, user is requested to input his credit card data.
After that, a user can choose a bicycle of any available type and from any available renting location.
Ordering process includes selection of chosen bicycle type (sport, mountain, ladies, etc.), picking a 
desired starting point, and selecting desired renting office to give bicycle back. Also, it is required to enter
desired renting time.
After the trip is complete, user returns bicycle, and finishes his order. Calculated amount of funds will be withdrawn from
his credit card account.
If there is not enough money on user`s account, it is possible to use account overdraft. If user overdraft exceeds maximum limit,
user is prohibited to make any more orders.
If user's history of orders counts 5 successful orders or more, his status is automatically improved.
User is allowed to enter his personal info, also pick desired avatar for his profile. User can edit his personal info at any time.
For user's comfort, application provides selection of 3 languages, including english, russian and belorussian.

Application also provides access for service administrator. Administrator has access for users profiles, can change their status,
 login, e-mail (but not the password!), also block users and disable them. Disabled users are blocked permanently.
 Administrator has access for the list of renting places, add new, rename or delete them.
 Administrator can edit bicycle types, add or delete, also adds, edits and deletes bicycles, and has authority to re-assign bicycles
 to any other renting place.

Application is deployed on local Tomcat web server (ver.9.0), created using Maven framework.
Application's data is stored on MySQL database server (ver.5.5). All SQL scripts required to create and maintain database are provided.
Application is using:
    - one web servlet to process http requests;
    - thread-safe connection pool;
    - data access objects layer, separated from application logic;
    - java servlet pages for view creation;
    - required filters and own exceptions;
    - JSTL tags for proper data visualisation;
    - two-layered validation of input data: on web page (client) and using Validator class (server);
    - UTF-8 encoding;
    - TestNG-based tests for data access layer;
    - Log4j2 logging technology to create logs; 