SELECT id, password, status, blocked, email, role FROM bicycles.user WHERE login = ?;
SELECT login, password, status, blocked, email, role FROM bicycles.user WHERE id = ?;
SELECT id, login, password, status, blocked, email, role FROM bicycles.user;
INSERT INTO bicycles.user (login, password, status, blocked, email, role) VALUES (?, ?, ?, ?, ?, ?);
UPDATE bicycles.user SET login = ?, password = ?, status = ?, blocked = ?, email = ? WHERE id = ?;
DELETE FROM bicycles.user WHERE id = ?;