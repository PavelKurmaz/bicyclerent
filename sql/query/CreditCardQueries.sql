SELECT cardnumber, balance, overdraft FROM bicycles.creditcard WHERE user_id = ?;
SELECT cardnumber, balance, overdraft, user_id FROM bicycles.creditcard;
INSERT INTO bicycles.creditcard (cardnumber, balance, overdraft,user_id) VALUES (?, ?, ?, ?);
UPDATE bicycles.creditcard SET cardnumber = ?, balance = ?, overdraft = ? WHERE user_id = ?;
DELETE FROM bicycles.creditcard WHERE user_id = ?;