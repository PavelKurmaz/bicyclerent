SELECT estimate, date, start, finish, status, user_id, bicycle_id FROM bicycles.order WHERE id = ?;
SELECT id, estimate, date, start, finish, status, user_id, bicycle_id FROM bicycles.order;
INSERT INTO bicycles.order (estimate, date, start, finish, status, user_id, bicycle_id) VALUES (?, ?, ?, ?, ?, ?, ?);
UPDATE bicycles.order SET estimate = ?, date = ?, start = ?, finish = ?, status = ?, user_id = ?, bicycle_id = ? WHERE id = ?;
DELETE FROM bicycles.user WHERE id = ?;