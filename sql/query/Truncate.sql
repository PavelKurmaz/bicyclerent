TRUNCATE TABLE bicycles.user;
TRUNCATE TABLE bicycles.rentplace;
TRUNCATE TABLE bicycles.type;
TRUNCATE TABLE bicycles.bicycle;
TRUNCATE TABLE bicycles.order;
TRUNCATE TABLE bicycles.creditcard;
TRUNCATE TABLE bicycles.profile;