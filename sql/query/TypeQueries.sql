SELECT description FROM bicycles.type WHERE id = ?;
SELECT id, description FROM bicycles.type;
INSERT INTO bicycles.type (description) VALUES (?);
UPDATE bicycles.type SET description = ? WHERE id = ?;
DELETE FROM bicycles.type WHERE id = ?;