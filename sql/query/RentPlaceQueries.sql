SELECT district FROM bicycles.rentplace WHERE id = ?;
SELECT id, district FROM bicycles.rentplace;
INSERT INTO bicycles.rentplace (district) VALUES (?);
UPDATE bicycles.rentplace SET district = ? WHERE id = ?;
DELETE FROM bicycles.rentplace WHERE id = ?;