SELECT firstName, lastName, address, phone FROM bicycles.profile WHERE user_id = ?;
SELECT firstName, lastName, address, phone, user_id FROM bicycles.profile;
INSERT INTO bicycles.profile (firstName, lastName, address, phone, user_id) VALUES (?, ?, ?, ?, ?);
UPDATE bicycles.profile SET firstName = ?, lastName = ?, address = ?, phone = ? WHERE user_id = ?;
DELETE FROM bicycles.profile WHERE user_id = ?;