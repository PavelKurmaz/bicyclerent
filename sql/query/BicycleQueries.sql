SELECT rate, type_id, rentplace_id, description FROM bicycles.bicycle WHERE id = ?;
SELECT id, rate, type_id, rentplace_id, description FROM bicycles.bicycle;
INSERT INTO bicycles.bicycle (type_id, rentplace_id, description) VALUES (?, ?, ?);
UPDATE bicycles.bicycle SET type_id = ?, rentplace_id = ?, description=? WHERE id = ?;
DELETE FROM bicycles.type WHERE id = ?;