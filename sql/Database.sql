CREATE SCHEMA IF NOT EXISTS `bicycles` DEFAULT CHARACTER SET utf8 ;
USE `bicycles` ;

CREATE TABLE IF NOT EXISTS `bicycles`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) UNIQUE NOT NULL ,
  `password` VARCHAR(45) NOT NULL,
  `status` ENUM('regular', 'silver', 'gold', 'weak') NOT NULL,
  `blocked` TINYINT(1) NOT NULL,
  `disabled` TINYINT(1) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `role` TINYINT(1) NOT NULL,
  `path` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`creditcard` (
  `cardnumber` VARCHAR(16) UNIQUE NOT NULL,
  `balance` DECIMAL NOT NULL,
  `overdraft` DECIMAL NOT NULL,
  `user_id` INT UNIQUE NOT NULL,
  INDEX `fk_creditcard_user_idx` (`user_id` ASC),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_creditcard_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `bicycles`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NOT NULL,
  `path` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`rentplace` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `district` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`bicycle` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rate` DECIMAL NOT NULL,
  `type_id` INT NOT NULL,
  `rentplace_id` INT NOT NULL,
  `model` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bicycle_type_idx` (`type_id` ASC),
  INDEX `fk_bicycle_rentplace_idx` (`rentplace_id` ASC),
  CONSTRAINT `fk_bicycle_type`
  FOREIGN KEY (`type_id`)
  REFERENCES `bicycles`.`type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE ,
  CONSTRAINT `fk_bicycle_rentplace`
  FOREIGN KEY (`rentplace_id`)
  REFERENCES `bicycles`.`rentplace` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE )
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `estimate` INT NOT NULL,
  `user_id` INT NOT NULL,
  `date` DATE NOT NULL,
  `bicycle_id` INT NOT NULL,
  `start` INT NOT NULL,
  `finish` INT NOT NULL,
  `status` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_user_idx` (`user_id` ASC),
  INDEX `fk_order_bicycle_idx` (`bicycle_id` ASC),
  INDEX `fk_order_rentplace1_idx` (`start` ASC),
  INDEX `fk_order_rentplace2_idx` (`finish` ASC),
  CONSTRAINT `fk_order_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `bicycles`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_bicycle`
  FOREIGN KEY (`bicycle_id`)
  REFERENCES `bicycles`.`bicycle` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE ,
  CONSTRAINT `fk_order_rentplace1`
  FOREIGN KEY (`start`)
  REFERENCES `bicycles`.`rentplace` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE ,
  CONSTRAINT `fk_order_rentplace2`
  FOREIGN KEY (`finish`)
  REFERENCES `bicycles`.`rentplace` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE )
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `bicycles`.`profile` (
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(16) NOT NULL,
  `user_id` INT UNIQUE NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_profile_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `bicycles`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;