DROP TABLE IF EXISTS bicycles.user;
DROP TABLE IF EXISTS bicycles.rentplace;
DROP TABLE IF EXISTS bicycles.type;
DROP TABLE IF EXISTS bicycles.bicycle;
DROP TABLE IF EXISTS bicycles.order;
DROP TABLE IF EXISTS bicycles.creditcard;
DROP TABLE IF EXISTS bicycles.profile;

DROP SCHEMA IF EXISTS `bicycles`;