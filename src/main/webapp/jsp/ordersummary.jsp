<jsp:useBean id="rentplace" scope="request" type="java.lang.String"/>
<jsp:useBean id="user" scope="session" type="by.training.bicyclerent.entity.User"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <h3 align="center"><fmt:message key="rentplace.summary"/>${rentplace}</h3>
    <br>

    <div class="row">
        <div class="col-lg-2"><fmt:message key="order.number"/></div>
        <div class="col-lg-2"><fmt:message key="date"/></div>
        <div class="col-lg-2"><fmt:message key="bicycle"/></div>
        <div class="col-lg-2"><fmt:message key="cost"/></div>
    </div>
    <hr>
    <div class="container">
        <jsp:useBean id="bicycles" scope="request" type="java.util.List"/>
        <jsp:useBean id="orders" scope="request" type="java.util.List"/>
        <c:forEach items="${orders}" var="order">
            <div class="row">
                <div class="col-lg-2">
                        ${order.id}
                </div>
                <div class="col-lg-2">
                        ${order.date}
                </div>
                <c:set var="count" value="0"/>
                <c:forEach items="${bicycles}" var="bicycle">
                    <c:if test="${bicycle.id == order.bicycleId && count == '0'}">
                        <div class="col-lg-2">
                                ${bicycle.model}
                        </div>
                        <div class="col-lg-2">
                                ${bicycle.rate * order.estimate}
                        </div>
                        <c:set var="count" value="1"/>
                    </c:if>
                </c:forEach>
            </div>
            <hr>
        </c:forEach>
        <br>
        <div align="left">
            <fmt:message key="summary"/>: ${summary}
        </div>
    </div>
</div>
</body>
</html>