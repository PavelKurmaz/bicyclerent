<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <form class="form-horizontal" method="post" action="action?command=signup">
        <fieldset>

            <!-- Form Name -->
            <legend><fmt:message key="user.registration"/></legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="login"><fmt:message key="user.login"/></label>
                <div class="col-lg-4">
                    <input id="login" name="login" type="text" placeholder="login 5-20 symbols"
                           class="form-control input-md" required pattern="[a-zA-Z0-9]{5,20}">
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="password"><fmt:message key="user.password"/></label>
                <div class="col-lg-4">
                    <input id="password" name="password" type="password" placeholder="password 5-20 symbols"
                           class="form-control input-md" required pattern="[a-zA-Z0-9]{5,20}">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="email"><fmt:message key="user.email"/></label>
                <div class="col-lg-4">
                    <input id="email" name="email" type="email" placeholder="enter e-mail here"
                           class="form-control input-md" required>
                </div>
            </div>

            <!-- Button -->
            <div class="col-md-4">
                <button id="Submit" name="Submit" class="btn btn-primary"><fmt:message key="user.signup"/></button>
            </div>
            <br>
            <div class="col-lg-4">
                <button id="clear" name="clear" type="reset" class="btn btn-primary"><fmt:message key="clear"/></button>
            </div>

        </fieldset>
    </form>
</div>
</body>
</html>




