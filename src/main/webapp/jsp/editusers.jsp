<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"><fmt:message key="user.login"/></div>
            <div class="col-lg-3"><fmt:message key="user.email"/></div>
            <div class="col-lg-2"><fmt:message key="user.status"/></div>
            <div class="col-lg-1"><fmt:message key="user.blocked"/></div>
            <div class="col-lg-1"><fmt:message key="user.disabled"/></div>
        </div>
    </div>
    <hr>
    <div class="container">
        <jsp:useBean id="users" scope="request" type="java.util.List"/>
        <c:forEach items="${users}" var="user">
            <form class="update-user-${user.id}" action="action?command=editusers" method=post>
                <div class="row">
                    <input name="id" type="hidden" value="${user.id}"/>
                    <input name="password" type="hidden" value="${user.password}"/>
                    <div class="col-lg-3">
                        <input id="login" class="form-control input-md" name="login"
                               value="${user.login}"/>
                    </div>
                    <div class="col-lg-3">
                        <input id="email" class="form-control input-md" name="email"
                               value="${user.email}"/>
                    </div>
                    <div class="col-lg-2">
                        <select id="status" name="status" class="form-control">
                            <option value="weak">Weak</option>
                            <option value="regular">Regular</option>
                            <option value="silver">Silver</option>
                            <option value="gold">Gold</option>
                            <option selected="${user.status}">${user.status}</option>
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <c:choose>
                            <c:when test="${user.blocked == false}">
                                <input type="checkbox" name="blocked" value="${user.id}">
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="blocked" value="${user.id}" checked>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-lg-1">
                        <c:choose>
                            <c:when test="${user.disabled == false}">
                                <input type="checkbox" name="disabled" value="${user.id}">
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="disabled" value="${user.id}" checked>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <button id="Update" value="Update" name="Update" class="btn btn-success col-lg-1">
                        <fmt:message key="update"/>
                    </button>
                    <button id="Delete" value="Delete" name="Delete" class="btn btn-danger col-lg-1">
                        <fmt:message key="delete"/>
                    </button>
                </div>
            </form>
            <br/>
        </c:forEach>
    </div>
    <div align="center">
        <jsp:useBean id="pagenumber" scope="session" type="java.lang.Integer"/>
        <c:if test="${pagenumber.intValue() > 1}">
            <a href="action?command=editusers&Previous=${pagenumber.intValue() - 1}">${pagenumber.intValue() - 1}</a>
        </c:if>
        <b> ${pagenumber} </b>
        <jsp:useBean id="maxpages" scope="request" type="java.lang.Integer"/>
        <c:if test="${maxpages.intValue() > pagenumber.intValue()}"><a
                href="action?command=editusers&Next=${pagenumber.intValue() + 1}">${pagenumber.intValue() + 1}</a>
        </c:if>
    </div>
</div>
</body>
</html>