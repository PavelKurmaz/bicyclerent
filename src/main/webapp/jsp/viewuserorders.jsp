<jsp:useBean id="user" scope="session" type="by.training.bicyclerent.entity.User"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <h3 align="center"><fmt:message key="user"/> ${user.login} <fmt:message key="orders"/></h3>
    <br>

    <div class="row">
        <div class="col-lg-1"><fmt:message key="order.number"/></div>
        <div class="col-lg-2"><fmt:message key="bicycles.renthours"/></div>
        <div class="col-lg-1"><fmt:message key="date"/></div>
        <div class="col-lg-2"><fmt:message key="bicycle"/></div>
        <div class="col-lg-2"><fmt:message key="bicycles.rate"/></div>
        <div class="col-lg-1"><fmt:message key="cost"/></div>
        <div class="col-lg-2"><fmt:message key="order.status"/></div>
    </div>
    <hr>
    <div class="container">
        <h3 align="center">${userStatusMessage}</h3>
        <br>
        <jsp:useBean id="bicycles" scope="request" type="java.util.List"/>
        <jsp:useBean id="orders" scope="request" type="java.util.List"/>
        <c:forEach items="${orders}" var="order">
            <form class="edit_orders_${user.id}" action="action?command=viewuserorders" method=post>
                <div class="row">
                    <input name="id" type="hidden" value="${order.id}"/>
                    <div class="col-lg-1">
                            ${order.id}
                    </div>
                    <div class="col-lg-2">
                            ${order.estimate}
                    </div>
                    <div class="col-lg-1">
                            ${order.date}
                    </div>
                    <c:set var="count" value="0"/>
                    <c:forEach items="${bicycles}" var="bicycle">
                        <c:if test="${bicycle.id == order.bicycleId && count == '0'}">
                            <div class="col-lg-2">
                                    ${bicycle.model}
                            </div>
                            <div class="col-lg-2">
                                    ${bicycle.rate}
                            </div>
                            <div class="col-lg-1">
                                    ${bicycle.rate * order.estimate}
                            </div>
                            <c:set var="count" value="1"/>
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${order.status == true}">
                            <div class="col-lg-2">
                                <fmt:message key="order.notcompleted"/>
                            </div>
                            <button type="submit" value="finish" name="finish" class="btn btn-success col-lg-1">
                                <fmt:message key="order.finish"/>
                            </button>
                        </c:when>
                        <c:otherwise>
                            <div class="col-lg-2">
                                <fmt:message key="order.completed"/>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <hr>
            </form>
        </c:forEach>
    </div>
</div>
</body>
</html>