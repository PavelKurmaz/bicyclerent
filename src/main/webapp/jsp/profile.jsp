<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <form class="form-horizontal" method="post" action="action?command=profile" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend><fmt:message key="profile.legend"/></legend>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="firstName"><fmt:message key="user.firstName"/></label>
                <div class="col-lg-4">
                    <input id="firstName" name="firstName" type="text" class="form-control input-md"
                           required pattern="[a-zA-Z]{1,20}" placeholder="1-20 symbols" value="${profile.firstName}">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastName"><fmt:message key="user.lastName"/></label>
                <div class="col-lg-4">
                    <input id="lastName" name="lastName" type="text" class="form-control input-md"
                           required pattern="[a-zA-Z]{1,20}" placeholder="1-20 symbols" value="${profile.lastName}">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="address"><fmt:message key="user.address"/></label>
                <div class="col-lg-4">
                    <input id="address" name="address" type="text" class="form-control input-md" required pattern="[a-zA-Z]{,45}"
                           placeholder="1-45 symbols" value="${profile.address}">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-lg-4 control-label" for="phone"><fmt:message key="user.phone"/></label>
                <div class="col-lg-4">
                    <input id="phone" name="phone" type="text" class="form-control input-md" required pattern="[\+](375)\d{9}"
                           placeholder="+375_________" value="${profile.phone}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="image">Avatar</label>
                <input id ="image" type="file" name="image" class="form-control-file">
            </div>

            <!-- Button -->
            <div class="col-lg-4">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary"><fmt:message key="submit"/></button>
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>




