<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <p></p>
    <hr>
    <jsp:useBean id="types" scope="request" type="java.util.List"/>
    <c:forEach items="${types}" var="type">
        <form class="update" action="action?command=edittypes" method=post>
            <div class="row">
                <input name="id" type="hidden" value="${type.id}"/>
                <img src="${type.path}" height="70" width="70">
                <div class="col-lg-3">
                    <label class="col-lg-3 control-label" for="description"><fmt:message
                            key="bicycles.model"/></label>
                    <input id="description" class="form-control input-md" name="description"
                           value="${type.description}"/>
                </div>
                <button id="Update" value="Update" name="Update" class="btn btn-success col-lg-1">
                    <fmt:message key="update"/>
                </button>
                <button id="Delete" value="Delete" name="Delete" class="btn btn-danger col-lg-1">
                    <fmt:message key="delete"/>
                </button>
            </div>
        </form>
        <p></p>
    </c:forEach>
    <br>
    <div align="center">
        <jsp:useBean id="pagenumber" scope="session" type="java.lang.Integer"/>
        <c:if test="${pagenumber.intValue() > 1}"><a
                href="action?command=edittypes&Previous=${pagenumber.intValue() - 1}">${pagenumber.intValue() - 1}</a>
        </c:if>
        <b> ${pagenumber} </b>
        <jsp:useBean id="maxpages" scope="request" type="java.lang.Integer"/>
        <c:if test="${maxpages.intValue() > pagenumber.intValue()}"><a
                href="action?command=edittypes&Next=${pagenumber.intValue() + 1}">${pagenumber.intValue() + 1}</a>
        </c:if>
    </div>
    <br>
    <div class="form-group">
        <button id="add" class="btn btn-primary"><a class="btn btn-primary"
                                                    href="action?command=addtype"><fmt:message key="types.add"/></a>
        </button>
    </div>
    <br/>
</div>
</body>
</html>