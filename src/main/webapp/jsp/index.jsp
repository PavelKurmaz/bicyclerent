<%@ page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<jsp:useBean id="user" scope="session" type="by.training.bicyclerent.entity.User"/>
<jsp:useBean id="profile" scope="session" type="by.training.bicyclerent.entity.Profile"/>
<c:choose>
    <c:when test="${user!=null}">
        <div class="col-md-6 col-lg6 col-md-offset-2 col-lg-offset-2 toppad">
            <div class="panel-heading">
                <h3 class="panel-title">${user.login}</h3>
            </div>
            <div class="panel-body">
                <table class="table table-user-information">
                    <tbody>
                    <tr>
                        <td><fmt:message key="user.login"/></td>
                        <td>${user.login}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.email"/></td>
                        <td>${user.email}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.status"/></td>
                        <td>${user.status}</td>
                    </tr>
                    <c:if test="${profile != null}">
                        <tr>
                            <td><fmt:message key="user.firstName"/></td>
                            <td>${profile.firstName}</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="user.lastName"/></td>
                            <td>${profile.lastName}</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="user.address"/></td>
                            <td>${profile.address}</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="user.phone"/></td>
                            <td>${profile.phone}</td>
                        </tr>
                    </c:if>
                    <c:choose>
                        <c:when test="${user.role == 0}">
                            <tr>
                                <td><fmt:message key="type"/></td>
                                <td>User</td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>Profile type</td>
                                <td>Admin</td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <button type="submit" id="logout" class="btn btn-primary"><a class="btn btn-primary"
                                                                             href="action?command=login"><fmt:message
                        key="user.logout"/></a>
                </button>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="form-group">
            <button type="submit" id="logout2" class="btn btn-primary"><a class="btn btn-primary"
                                                                          href="action?command=login"><fmt:message
                    key="user.logout"/></a>
            </button>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>
