<jsp:useBean id="cost" scope="request" type="java.math.BigDecimal"/>
<jsp:useBean id="bicycle" scope="request" type="by.training.bicyclerent.entity.Bicycle"/>
<jsp:useBean id="order" scope="request" type="by.training.bicyclerent.entity.Order"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <fieldset>

        <!-- Form Name -->
        <legend align="center"><fmt:message key="user.orderconfirmed"/></legend>

        <div class="col-lg-10" align="center">
            Order #: ${order.id} confirmed! Bicycle Model ${bicycle.model} rented for ${order.estimate} hours.
            Total cost of order is ${cost}.
        </div>
        <hr>

        <div class="form-group">
            <button id="orders" class="btn btn-primary"><a class="btn btn-primary"
                                                           href="action?command=viewuserorders"><fmt:message
                    key="user.vieworders"/></a>
            </button>
        </div>
    </fieldset>
</div>
</body>
</html>




