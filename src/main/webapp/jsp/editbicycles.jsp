<jsp:useBean id="pagenumber" scope="session" type="java.lang.Integer"/>
<jsp:useBean id="bicycles" scope="request" type="java.util.List"/>
<jsp:useBean id="types" scope="request" type="java.util.List"/>
<jsp:useBean id="rentplaces" scope="request" type="java.util.List"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">Image</div>
            <div class="col-lg-2"><fmt:message key="type"/></div>
            <div class="col-lg-2"><fmt:message key="bicycles.model"/></div>
            <div class="col-lg-1"><fmt:message key="rate"/></div>
            <div class="col-lg-3"><fmt:message key="bicycles.location"/></div>
        </div>
    </div>
    <hr>
    <div class="container">
        <c:forEach items="${bicycles}" var="bicycle">
            <form class="update-user-${bicycle.id}" action="action?command=editbicycles" method=post>
                <div class="row">
                    <div class="col-lg-2">
                        <input name="id" type="hidden" value="${bicycle.id}"/>
                        <c:forEach items="${types}" var="type">
                            <c:if test="${bicycle.typeId == type.id}">
                                <img src="${type.path}" height="70" width="70">
                            </c:if>
                        </c:forEach>
                    </div>
                    <div class="col-lg-2">
                        <select id="type" name="type" class="form-control">
                            <c:forEach items="${types}" var="type">
                                <option value="${type.id}">${type.description}</option>
                            </c:forEach>

                            <c:forEach items="${types}" var="type">
                                <c:if test="${bicycle.typeId == type.id}">
                                    <option selected="" value="${type.id}">
                                        <hr>
                                            ${type.description}
                                    </option>
                                </c:if>
                            </c:forEach>

                        </select>
                    </div>
                    <div class="col-lg-2">
                        <input id="model" class="form-control input-md" name="model"
                               value="${bicycle.model}"/>
                    </div>
                    <div class="col-lg-1">
                        <input id="rate" class="form-control input-md" name="rate"
                               value="${bicycle.rate}"/>
                    </div>
                    <div class="col-lg-3">
                        <select id="rentplace" name="rentplace" class="form-control">
                            <c:forEach items="${rentplaces}" var="place">
                                <option value="${place.id}">${place.district}</option>
                            </c:forEach>
                            <c:forEach items="${rentplaces}" var="rentplace">
                                <c:if test="${bicycle.rentPlaceId == rentplace.id}">
                                    <option selected="" value="${rentplace.id}">
                                        <hr>
                                            ${rentplace.district}
                                    </option>
                                </c:if>
                            </c:forEach>

                        </select>
                    </div>
                    <button id="Update" value="Update" name="Update" class="btn btn-success col-lg-1">
                        <fmt:message key="update"/>
                    </button>
                    <button id="Delete" value="Delete" name="Delete" class="btn btn-danger col-lg-1">
                        <fmt:message key="delete"/>
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
        <br>
        <div align="center">
            <c:if test="${pagenumber.intValue() > 1}"><a
                    href="action?command=editbicycles&Previous=${pagenumber.intValue() - 1}">${pagenumber.intValue() - 1}</a>
            </c:if>
            <b> ${pagenumber} </b>
            <jsp:useBean id="maxpages" scope="request" type="java.lang.Integer"/>
            <c:if test="${maxpages.intValue() > pagenumber.intValue()}"><a
                    href="action?command=editbicycles&Next=${pagenumber.intValue() + 1}">${pagenumber.intValue() + 1}</a>
            </c:if>
        </div>
        <div class="form-group">
            <button type="submit" id="add" class="btn btn-primary"><a class="btn btn-primary"
                                                                      href="action?command=addbicycle"><fmt:message
                    key="bicycles.add"/></a>
            </button>
        </div>
    </div>
</div>
</body>
</html>