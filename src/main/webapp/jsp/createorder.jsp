<jsp:useBean id="type" scope="request" type="by.training.bicyclerent.entity.Type"/>
<jsp:useBean id="place" scope="request" type="by.training.bicyclerent.entity.RentPlace"/>
<jsp:useBean id="user" scope="session" type="by.training.bicyclerent.entity.User"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <h3 align="center">Select Bicycle to Order. Location: ${place.district}</h3>
    <hr>
    <div class="row">
        <div class="col-lg-2">${type.description}</div>
        <div class="col-lg-2"><fmt:message key="bicycles.model"/></div>
        <div class="col-lg-2"><fmt:message key="bicycles.rate"/></div>
        <div class="col-lg-2"><fmt:message key="bicycles.renthours"/></div>
        <div class="col-lg-3"><fmt:message key="bicycles.returnpoint"/></div>
    </div>
    <hr>
    <div class="container">

        <jsp:useBean id="bicycles" scope="request" type="java.util.List"/>
        <c:choose>
            <c:when test="${bicycles.size() != 0}">
                <c:forEach items="${bicycles}" var="bicycle">
                    <form class="form-action" action="action?command=createorder" method=post>
                        <div class="row">
                            <input name="bicycleId" type="hidden" value="${bicycle.id}"/>
                            <input name="start" type="hidden" value="${place.id}"/>
                            <div class="col-lg-2">
                                    <img src="${type.path}" height="70" width="70">
                            </div>
                            <div class="col-lg-2">
                                    ${bicycle.model}
                            </div>
                            <div class="col-lg-2">
                                    ${bicycle.rate}
                            </div>
                            <div class="col-lg-2">
                                <input id="estimate" name="estimate" type="text" placeholder="time in hours 1-24"
                                       class="form-control input-md" required pattern="[0-9]{1,2}">
                            </div>
                            <div class="col-lg-3">
                                <select id="finish" name="finish" class="form-control">
                                    <jsp:useBean id="places" scope="request" type="java.util.List"/>
                                    <c:forEach items="${places}" var="place">
                                        <option value="${place.id}">${place.district}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <button id="Rent" value="Rent" name="Rent" class="btn btn-success col-lg-1">
                                <fmt:message key="rent"/>
                            </button>
                        </div>
                    </form>
                    <p></p>
                </c:forEach>
                <br>
                <form class="form-action" action="action?command=preorder" method=post>
                    <div class="row">
                        <jsp:useBean id="pagenumber" scope="session" type="java.lang.Integer"/>
                        <c:if test="${pagenumber.intValue() > 1}">
                            <button id="Previous" value="${pagenumber.intValue() - 1}" name="Previous"
                                    class="btn btn-success col-lg-1">
                                    ${pagenumber.intValue() - 1}
                            </button>
                            <button id="Next" value="${pagenumber.intValue() + 1}" name="Next"
                                    class="btn btn-success col-lg-1">
                                    ${pagenumber.intValue() + 1}
                            </button>
                        </c:if>
                    </div>
                </form>
            </c:when>
            <c:otherwise>
                <div class="col-lg-6" align="center">
                    Sorry, no bicycles available now!
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>