<html>
<%@ include file="/include/head.htm" %>
<body class="p-3 mb-2 bg-secondary text-white">
<h1 align="center" color="blue">Welcome to Bicycle Rent Service!<span
        class="badge badge-secondary">(Training project)</span>
</h1>
<div align="center">
    <img src="img/welcome.jpg" class="img-fluid" alt="Responsive image" width="800" height="400">
</div>
<form class="form-horizontal" method="post" action="action?command=index">


    <div class="col-lg-4 col-lg-offset-2">
        <legend>Select Language</legend>
        <select id="lang" name="lang" class="form-control" title="Select Language">
            <option value="US">English</option>
            <option value="RU">Russian</option>
            <option value="BY">Belarussian</option>
        </select>
    </div>
    <p></p>
    <div class="col-lg-4">
        <button id="singlebutton" name="singlebutton" class="btn btn-primary">Select</button>
    </div>
</form>
</body>
</html>

