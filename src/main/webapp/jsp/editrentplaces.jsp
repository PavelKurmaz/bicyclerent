<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <div class="container">
        <div class="row">
            <div class=col-lg-3><fmt:message key="rentplace.rentofficedistrict"/></div>
        </div>
    </div>
    <hr>
    <div class="container">
        <jsp:useBean id="places" scope="request" type="java.util.List"/>
        <c:forEach items="${places}" var="place">
            <form class="update" action="action?command=editrentplaces" method=post>
                <div class="row">
                    <input name="id" type="hidden" value="${place.id}"/>
                    <div class=col-lg-3>
                        <input id="district" class="form-control input-md" name="district"
                               value="${place.district}"/>
                    </div>
                    <button id="Update" value="Update" name="Update" class="btn btn-success col-lg-1">
                        <fmt:message key="update"/>
                    </button>
                    <button id="Delete" value="Delete" name="Delete" class="btn btn-danger col-lg-1">
                        <fmt:message key="delete"/>
                    </button>
                </div>
            </form>
            <p></p>
        </c:forEach>
        <br>
        <div align="center">
            <jsp:useBean id="pagenumber" scope="session" type="java.lang.Integer"/>
            <c:if test="${pagenumber.intValue() > 1}"><a
                    href="action?command=editrentplaces&Previous=${pagenumber.intValue() - 1}">${pagenumber.intValue() - 1}</a>
            </c:if>
            <b> ${pagenumber} </b>
            <jsp:useBean id="maxpages" scope="request" type="java.lang.Integer"/>
            <c:if test="${maxpages.intValue() > pagenumber.intValue()}"><a
                    href="action?command=editrentplaces&Next=${pagenumber.intValue() + 1}">${pagenumber.intValue() + 1}</a>
            </c:if>
        </div>
        <br>
        <div class="form-group">
            <button type="submit" id="add" class="btn btn-primary"><a class="btn btn-primary"
                                                                      href="action?command=addrentplace"><fmt:message
                    key="rentplace.add"/></a>
            </button>
        </div>
    </div>
</div>
</body>
</html>