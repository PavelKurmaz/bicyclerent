<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <form class="form-horizontal" method="post" action="action?command=editbicycles">
        <fieldset>

            <!-- Form Name -->
            <legend><fmt:message key="bicycles.add"/></legend>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="rate"><fmt:message key="bicycles.rate"/></label>
                <div class="col-lg-4">
                    <input id="rate" name="rate" type="text" placeholder="enter renting rate"
                           required pattern="[0-9]{,2}" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="model"><fmt:message key="bicycles.model"/></label>
                <div class="col-lg-4">
                    <input id="model" name="model" type="text" placeholder="enter model"
                           required pattern="[A-Za-z]{,16}" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="type"><fmt:message key="type"/></label>
                <div class="col-lg-4">
                    <select id="type" name="type" class="form-control">
                        <jsp:useBean id="types" scope="request" type="java.util.List"/>
                        <c:forEach items="${types}" var="type">
                            <option value="${type.id}">${type.description}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="rentplace"><fmt:message key="bicycles.location"/></label>
                <div class="col-lg-4">
                    <select id="rentplace" name="rentplace" class="form-control">
                        <jsp:useBean id="places" scope="request" type="java.util.List"/>
                        <c:forEach items="${places}" var="place">
                            <option value="${place.id}">${place.district}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <!-- Button -->
            <div class="col-lg-4">
                <button id="Add" name="Add" class="btn btn-primary"><fmt:message key="add"/></button>
            </div>

        </fieldset>
    </form>
</div>
</body>
</html>




