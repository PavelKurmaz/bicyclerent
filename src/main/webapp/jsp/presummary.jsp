<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>

<div class="container">
    <jsp:useBean id="places" scope="request" type="java.util.List"/>

    <form class="update" action="action?command=summary" method=post>

        <div class="col-lg-6" align="center">
            <label class="col-lg-6" for="place"><fmt:message key="rentplace.selecttype"/></label>
            <select id="place" name="place" class="form-control">
                <c:forEach items="${places}" var="place">
                    <option value="${place.id}">${place.district}</option>
                </c:forEach>
            </select>
        </div>
        <br>
        <div class="col-lg-6" align="center">
            <button type="submit" name="district" class="btn btn-success col-lg-4">
                <fmt:message key="select"/>
            </button>
        </div>
        <br>
        <div class="col-lg-6" align="center">
            <button type="submit" name="summary" class="btn btn-success col-lg-4">
                <fmt:message key="summary.overall"/>
            </button>
        </div>
    </form>
</div>
</body>
</html>