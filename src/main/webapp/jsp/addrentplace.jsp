<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <form class="form-horizontal" method="post" action="action?command=editrentplaces">
        <fieldset>

            <!-- Form Name -->
            <legend><fmt:message key="rentplaces.add"/></legend>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="district">Type</label>
                <div class="col-lg-4">
                    <input id="district" name="district" type="text" placeholder="enter district name"
                           class="form-control input-md">
                </div>
            </div>

            <!-- Button -->
            <div class="col-md-4">
                <button id="Add" name="Add" class="btn btn-primary"><fmt:message key="add"/></button>
            </div>

        </fieldset>
    </form>
</div>
</body>
</html>




