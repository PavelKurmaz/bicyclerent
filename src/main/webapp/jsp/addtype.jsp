<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">

    <form class="form-horizontal" method="post" action="action?command=edittypes" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend><fmt:message key="types.add"/></legend>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="description"><fmt:message key="type"/></label>
                <div class="col-lg-4">
                    <input id="description" name="description" type="text" placeholder="enter type name"
                           class="form-control input-md">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="image">Picture</label>
                <input id ="image" type="file" name="image" class="form-control-file">
            </div>

            <!-- Button -->
            <div class="col-lg-2">
                <button id="Add" name="Add" class="btn btn-primary"><fmt:message key="add"/></button>
            </div>

        </fieldset>
    </form>
</div>
</body>
</html>




