<jsp:useBean id="errormessage" scope="request" type="java.lang.String"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <fieldset>

        <!-- Form Name -->
        <legend align="center"><fmt:message key="user.orderfailed"/></legend>

        <div class="col-lg-12" align="center">
            <b>${errormessage}</b>
        </div>
        <br>
        <div class="form-group">
            <button id="orders" class="btn btn-primary"><a class="btn btn-primary"
                                                           href="action?command=preorder"><fmt:message key="retry"/></a>
            </button>
        </div>
    </fieldset>
</div>
</body>
</html>




