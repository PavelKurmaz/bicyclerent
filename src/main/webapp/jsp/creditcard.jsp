<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<%@ include file="/include/head.htm" %>
<body>
<%@ include file="/include/menu.htm" %>
<div class="container">
    <jsp:useBean id="creditcard" scope="request" type="by.training.bicyclerent.entity.CreditCard"/>
    <c:choose>
        <c:when test="${creditcard.cardNumber == null}">
            <form class="form-horizontal" method="post" action="action?command=card">
                <fieldset>
                    <!-- Form Name -->
                    <legend><fmt:message key="user.creditcard.add"/></legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-1">
                                <input id="one" name="one" type="text" class="form-control input-md"
                                       required pattern="[0-9]{4}">
                            </div>

                            <div class="col-lg-1">
                                <input id="two" name="two" type="text" class="form-control input-md"
                                       required pattern="[0-9]{4}">
                            </div>
                            <div class="col-lg-1">
                                <input id="three" name="three" type="text" class="form-control input-md"
                                       required pattern="[0-9]{4}">
                            </div>
                            <div class="col-lg-1">
                                <input id="four" name="four" type="text" class="form-control input-md"
                                       required pattern="[0-9]{4}">
                            </div>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="col-lg-4">
                        <button id="singlebutton" name="singlebutton" class="btn btn-primary"><fmt:message
                                key="submit"/></button>
                    </div>
                </fieldset>
            </form>
        </c:when>
        <c:otherwise>
            <div class="form-group">
                <table class="table table-user-information">
                    <tbody>
                    <tr>
                        <td><fmt:message key="user.creditcard.number"/></td>
                        <td>${creditcard.cardNumber}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.creditcard.balance"/></td>
                        <td>${creditcard.balance}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="user.creditcard.overdraft"/></td>
                        <td>${creditcard.overdraft}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>




