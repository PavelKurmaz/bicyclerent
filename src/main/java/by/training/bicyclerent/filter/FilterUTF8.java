package by.training.bicyclerent.filter;

import javax.servlet.*;
import java.io.IOException;
/**
 * Filter class to check Request data encoding.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class FilterUTF8 implements Filter {
    /**
     * Private String value for encode field.
     */
    private String encode;
    /**
     * Overridden initialization method for Filter class.
     * @param filterConfig - filter configuration object.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        encode = filterConfig.getInitParameter("encode");
    }
    /**
     * Overridden main processing method for Filter class.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @param filterChain - FilterChain object.
     * @throws IOException - I\O exception.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
            throws IOException, ServletException {
        String encoding = request.getCharacterEncoding();
        if (encoding == null || !encoding.equals(encode)) {
            request.setCharacterEncoding(encode);
            encoding = response.getCharacterEncoding();
        }
        if (encoding == null || !encoding.equals(encode)) {
            response.setCharacterEncoding(encode);
        }
        filterChain.doFilter(request, response);
    }
    /**
     * Overridden finishing method for Filter lifecycle.
     */
    @Override
    public void destroy() { //Empty overridden method.

    }
}
