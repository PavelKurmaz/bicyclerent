package by.training.bicyclerent.filter;

import by.training.bicyclerent.action.Actions;
import by.training.bicyclerent.action.AdminAction;
import by.training.bicyclerent.entity.User;
import org.apache.commons.lang3.EnumUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter class to check logged user role type.
 * Prohibits unsupported operations.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class RoleFilter implements Filter {
    /**
     * Private role id value.
     */
    private int roleId;

    /**
     * Overridden initialization method for Filter class.
     * @param filterConfig - filter configuration object.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        roleId = Integer.parseInt(filterConfig.getInitParameter("role"));
    }

    /**
     * Overridden main processing method for Filter class.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @param filterChain - FilterChain object.
     * @throws IOException - I\O exception.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String command = httpServletRequest.getParameter("command");
        User user = (User) httpServletRequest.getSession().getAttribute("user");
        if (command != null && user != null) {
            if (EnumUtils.isValidEnum(AdminAction.class, command.toUpperCase())
                    && user.getId() != roleId) {
                request.setAttribute("errmessage",
                        "Wrong command, or illegal access!");
                RequestDispatcher requestDispatcher =
                        httpServletRequest.getRequestDispatcher(
                                Actions.ERROR.jsp);
                requestDispatcher.forward(request, response);
            }
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Overridden finishing method for Filter lifecycle.
     */
    @Override
    public void destroy() { //Empty overridden method.

    }
}
