package by.training.bicyclerent.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter class to check logged user role type.
 * Prohibits unsupported operations.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class ScriptFilter implements Filter {
    /**
     * String value to search in Request.
     */
    private String searchValue;

    /**
     * Overridden initialization method for Filter class.
     * @param filterConfig - filter configuration object.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        searchValue = filterConfig.getInitParameter("search");
    }

    /**
     * Overridden main processing method for Filter class.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @param filterChain - FilterChain object.
     * @throws IOException - I\O exception.
     * @throws ServletException - Servlet Exception.
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String[] paramValues = httpServletRequest.getParameterValues("action");
        if (paramValues != null) {
            for (String parameter : paramValues) {
                if (parameter.contains(searchValue)) {
                    request.setAttribute("errmessage",
                            "Possible Script Injection in parameter " + parameter);
                    httpServletRequest.getRequestDispatcher("/jsp/usererror.jsp")
                            .forward(request, response);
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Overridden finishing method for Filter lifecycle.
     */
    @Override
    public void destroy() { //Empty overridden method.
    }
}
