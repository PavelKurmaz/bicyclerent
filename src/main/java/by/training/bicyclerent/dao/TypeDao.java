package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.Type;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for Type data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class TypeDao implements Dao<Type> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " description, path FROM type WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_BY_DESC = "SELECT"
            + " id, path FROM type WHERE description = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " id, description, path FROM type";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO"
            + " type (description, path) VALUES (?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " type SET description = ?, path = ? WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE"
            + " FROM type WHERE id = ?";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;
    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public TypeDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO read method.
     * @param id - entity ID value to read DB data by.
     * @return - read Type Entity.
     */
    @Override
    public Type read(final int id) {
        Type type = new Type();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    type.setId(id);
                    type.setDescription(set.getString("description"));
                    type.setPath(set.getString("path"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return type;
    }

    public Type readByDescription(final String description) {
        Type type = new Type();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_BY_DESC)) {
            statement.setString(1, description);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    type.setId(set.getInt("id"));
                    type.setDescription(description);
                    type.setPath(set.getString("path"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return type;
    }

    /**
     * DAO Create method.
     * @param type - Entity to store in DB.
     * @return - generated int value of Entity ID.
     */
    @Override
    public int create(final Type type) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, type.getDescription());
            statement.setString(2, type.getPath());
            statement.executeUpdate();
            try (ResultSet set = statement.getGeneratedKeys()) {
                if (set.next()) {
                    id = set.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param type - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final Type type) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setString(1, type.getDescription());
            statement.setString(2, type.getPath());
            statement.setInt(3, type.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param type - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final Type type) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, type.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of read Entities.
     */
    @Override
    public List<Type> getAll() {
        List<Type> types = new ArrayList<>();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_ALL)) {
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Type type = new Type();
                    type.setId(set.getInt("id"));
                    type.setDescription(set.getString("description"));
                    type.setPath(set.getString("path"));
                    types.add(type);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return types;
    }
}
