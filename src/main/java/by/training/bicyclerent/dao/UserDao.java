package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for User data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class UserDao implements Dao<User> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_BY_NAME = "SELECT"
            + " id, password, status, blocked, disabled, email,"
            + " role, path FROM user WHERE login = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " login, password, status, blocked, disabled,"
            + " email, role, path FROM user WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " id, login, password, status, blocked, disabled,"
            + " email, role, path FROM user";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO"
            + " user (login, password, status, blocked,"
            + " disabled, email, role, path) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " user SET login = ?, password = ?,"
            + " status = ?, blocked = ?, disabled = ?,"
            + " email = ?, path = ? WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE"
            + " FROM user WHERE id = ?";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;
    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public UserDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO Read method.
     * @param id - entity ID value to read DB data by.
     * @return - read User Entity.
     */
    @Override
    public User read(final int id) {
        User user = new User();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    user.setId(id);
                    user.setLogin(set.getString("login"));
                    user.setPassword(set.getString("password"));
                    user.setEmail(set.getString("email"));
                    user.setStatus(Status.valueOf(
                            set.getString("status").toUpperCase()));
                    user.setBlocked(set.getBoolean("blocked"));
                    user.setDisabled(set.getBoolean("disabled"));
                    user.setRole(set.getInt("role"));
                    user.setPassword(set.getString("path"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        if (user.isDisabled()) {
            return new User();
        }
        return user;
    }

    /**
     * Upgraded Read method.
     * @param login - String value to select Entities by.
     * @return - read User Entity.
     */
    public User readByName(final String login) {
        User user = new User();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_BY_NAME)) {
            statement.setString(1, login);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    user.setId(set.getInt("id"));
                    user.setLogin(login);
                    user.setPassword(set.getString("password"));
                    user.setEmail(set.getString("email"));
                    user.setStatus(Status.valueOf(
                            set.getString("status").toUpperCase()));
                    user.setBlocked(set.getBoolean("blocked"));
                    user.setDisabled(set.getBoolean("disabled"));
                    user.setRole(set.getInt("role"));
                    user.setPath(set.getString("path"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        if (user.isDisabled()) {
            return new User();
        }
        return user;
    }

    /**
     * DAO Create method.
     * @param user - Entity to store in DB.
     * @return - generated int value of Entity ID.
     */
    @Override
    public int create(final User user) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getStatus().getValue().toLowerCase());
            statement.setBoolean(4, user.isBlocked());
            statement.setBoolean(5, user.isDisabled());
            statement.setString(6, user.getEmail());
            statement.setInt(7, user.getRole());
            statement.setString(8, user.getPath());
            statement.executeUpdate();
            try (ResultSet set = statement.getGeneratedKeys()) {
                if (set.next()) {
                    id = set.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param user - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final User user) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getStatus().getValue());
            statement.setBoolean(4, user.isBlocked());
            statement.setBoolean(5, user.isDisabled());
            statement.setString(6, user.getEmail());
            statement.setInt(8, user.getId());
            statement.setString(7, user.getPath());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param user - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final User user) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, user.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of read Entities.
     */
    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    User user = new User();
                    user.setId(set.getInt("id"));
                    user.setLogin(set.getString("login"));
                    user.setPassword(set.getString("password"));
                    user.setEmail(set.getString("email"));
                    user.setStatus(Status.valueOf(
                            set.getString("status").toUpperCase()));
                    user.setBlocked(set.getBoolean("blocked"));
                    user.setRole(set.getInt("role"));
                    user.setDisabled(set.getBoolean("disabled"));
                    user.setPath(set.getString("path"));
                    if (!user.isDisabled()) {
                        users.add(user);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return users;
    }
}
