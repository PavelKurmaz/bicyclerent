package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for Order data access object.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class OrderDao implements Dao<Order> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " estimate, date, start, finish, status, user_id"
            + ", bicycle_id FROM `order` WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " id, estimate, date, start, finish, status, user_id,"
            + " bicycle_id FROM `order`";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO"
            + " `order` (estimate, date, start, finish,"
            + " status, user_id, bicycle_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " `order` SET estimate = ?, date = ?,"
            + " start = ?, finish = ?, status = ?, user_id = ?,"
            + " bicycle_id = ? WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE FROM"
            + " `order` WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SELECT_BY_USER = "SELECT"
            + " id, estimate, date, start, finish, status,"
            + " bicycle_id FROM `order` WHERE user_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SELECT_COMPLETED = "SELECT"
            + " id, estimate, date, start, finish"
            + " bicycle_id FROM `order` WHERE user_id = ? AND status = 1";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SELECT_BY_PLACE = "SELECT"
            + " id, user_id, estimate, date, finish, status,"
            + " bicycle_id FROM `order` WHERE start = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String CREATE_VIEW_DISTRICT = "CREATE"
            + " OR REPLACE VIEW district_summary AS\n"
            + "SELECT `order`.estimate, bicycle.rate,"
            + " `order`.estimate * bicycle.rate AS cost FROM"
            + " `order`, bicycle WHERE `order`.bicycle_id ="
            + " bicycle.id AND "
            + "`order`.id IN (SELECT id FROM `order` WHERE `order`.start = ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DISTRICT_SUM = "SELECT"
            + " SUM(cost) FROM district_summary";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SUM = "SELECT SUM(cost) FROM summary";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String CREATE_VIEW_SUMMARY = "CREATE OR REPLACE VIEW"
            + " summary AS\n" +
            " SELECT `order`.estimate, bicycle.rate,"
            + " `order`.estimate * bicycle.rate AS"
            + " cost FROM `order`, bicycle WHERE"
            + " `order`.bicycle_id = bicycle.id";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;

    /**
     * Public class constructor.
     *
     * @param connection - Connection class object.
     */
    public OrderDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * Method used in App logic to calculate and return cost summary.
     *
     * @return - BigDecimal value of calculated sum.
     */
    public BigDecimal getSum() {
        double sum = 0;
        try (Statement statement = connection.createStatement()) {
            statement.execute(CREATE_VIEW_SUMMARY);
            statement.execute(SUM);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    sum = set.getDouble(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return BigDecimal.valueOf(sum);
    }

    /**
     * Method used in App logic to calculate and return cost
     * summary for RentPlace.
     *@param rentPlaceId - ID to select Entities by.
     * @return - BigDecimal value of calculated sum.
     */
    public BigDecimal getDistrictSum(final int rentPlaceId) {
        double sum = 0;
        try (PreparedStatement statement =
                     connection.prepareStatement(CREATE_VIEW_DISTRICT)) {
            statement.setInt(1, rentPlaceId);
            statement.execute();
            statement.execute(DISTRICT_SUM);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    sum = set.getDouble(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return BigDecimal.valueOf(sum);
    }

    /**
     * DAO read method.
     *
     * @param id - entity ID value to read DB data by.
     * @return - read Order Entity.
     */
    @Override
    public Order read(final int id) {
        Order order = new Order();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    order.setId(id);
                    order.setBicycleId(set.getInt("bicycle_id"));
                    order.setUserId(set.getInt("user_id"));
                    order.setDate(set.getDate("date"));
                    order.setEstimate(set.getInt("estimate"));
                    order.setStatus(set.getBoolean("status"));
                    order.setStart(set.getInt("start"));
                    order.setFinish(set.getInt("finish"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return order;
    }

    /**
     * DAO Create method.
     *
     * @param order - Entity to store in DB.
     * @return - int value of generated Entity ID.
     */
    @Override
    public int create(final Order order) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, order.getEstimate());
            statement.setDate(2, order.getDate());
            statement.setInt(3, order.getStart());
            statement.setInt(4, order.getFinish());
            statement.setBoolean(5, order.isStatus());
            statement.setInt(6, order.getUserId());
            statement.setInt(7, order.getBicycleId());
            statement.executeUpdate();
            try (ResultSet set = statement.getGeneratedKeys()) {
                if (set.next()) {
                    id = set.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     *
     * @param order - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final Order order) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setInt(1, order.getEstimate());
            statement.setDate(2, order.getDate());
            statement.setInt(3, order.getStart());
            statement.setInt(4, order.getFinish());
            statement.setBoolean(5, order.isStatus());
            statement.setInt(6, order.getUserId());
            statement.setInt(7, order.getBicycleId());
            statement.setInt(8, order.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     *
     * @param order - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final Order order) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, order.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     *
     * @return - List of Read Entities.
     */
    @Override
    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Order order = new Order();
                    order.setId(set.getInt("id"));
                    order.setBicycleId(set.getInt("bicycle_id"));
                    order.setUserId(set.getInt("user_id"));
                    order.setDate(set.getDate("date"));
                    order.setEstimate(set.getInt("estimate"));
                    order.setStatus(set.getBoolean("status"));
                    order.setStart(set.getInt("start"));
                    order.setFinish(set.getInt("finish"));
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    /**
     * Upgraded GetAll method.
     *
     * @param userId - ID value to select Entities by.
     * @return - List of read Entities.
     */
    public List<Order> getByUserId(final int userId) {
        List<Order> orders = new ArrayList<>();
        try (PreparedStatement statement =
                     connection.prepareStatement(SELECT_BY_USER)) {
            statement.setInt(1, userId);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Order order = new Order();
                    order.setId(set.getInt("id"));
                    order.setBicycleId(set.getInt("bicycle_id"));
                    order.setUserId(userId);
                    order.setDate(set.getDate("date"));
                    order.setEstimate(set.getInt("estimate"));
                    order.setStatus(set.getBoolean("status"));
                    order.setStart(set.getInt("start"));
                    order.setFinish(set.getInt("finish"));
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    /**
     * Upgraded GetAll method.
     *
     * @param userId - ID value to select Entities by.
     * @return - List of read Entities.
     */
    public List<Order> getCompleted(final int userId) {
        List<Order> orders = new ArrayList<>();
        try (PreparedStatement statement =
                     connection.prepareStatement(SELECT_COMPLETED)) {
            statement.setInt(1, userId);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Order order = new Order();
                    order.setId(set.getInt("id"));
                    order.setBicycleId(set.getInt("bicycle_id"));
                    order.setUserId(userId);
                    order.setDate(set.getDate("date"));
                    order.setEstimate(set.getInt("estimate"));
                    order.setStatus(false);
                    order.setStart(set.getInt("start"));
                    order.setFinish(set.getInt("finish"));
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    /**
     * Upgraded GetAll method.
     *
     * @param placeId - ID value to select Entities by.
     * @return - List of read Entities.
     */
    public List<Order> getByPlaceId(final int placeId) {
        List<Order> orders = new ArrayList<>();
        try (PreparedStatement statement =
                     connection.prepareStatement(SELECT_BY_PLACE)) {
            statement.setInt(1, placeId);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Order order = new Order();
                    order.setId(set.getInt("id"));
                    order.setBicycleId(set.getInt("bicycle_id"));
                    order.setUserId(set.getInt("user_id"));
                    order.setDate(set.getDate("date"));
                    order.setEstimate(set.getInt("estimate"));
                    order.setStatus(set.getBoolean("status"));
                    order.setStart(placeId);
                    order.setFinish(set.getInt("finish"));
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }
}
