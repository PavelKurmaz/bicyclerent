/**
 * Package contains Data Access Object classes for BicycleRent Project.
 * Provides Data creation, reading, updating, deleting (CRUD methods).
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.dao;
