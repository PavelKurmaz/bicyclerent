package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for Profile data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class ProfileDao implements Dao<Profile> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " firstname, lastname, address, phone FROM"
            + " profile WHERE user_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " firstname, lastname, address, phone, user_id FROM"
            + " profile";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO"
            + " profile (firstname, lastname, address,"
            + " phone, user_id) VALUES (?, ?, ?, ?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " profile SET firstname = ?, lastname = ?,"
            + " address = ?, phone = ? WHERE user_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE FROM"
            + " profile WHERE user_id = ?";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;
    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public ProfileDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO Read method.
     * @param id - entity ID value to read DB data by.
     * @return - read Profile Entity.
     */
    @Override
    public Profile read(final int id) {
        Profile profile = new Profile();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    profile.setUserId(id);
                    profile.setFirstName(set.getString("firstname"));
                    profile.setLastName(set.getString("lastname"));
                    profile.setAddress(set.getString("address"));
                    profile.setPhone(set.getString("phone"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return profile;
    }

    /**
     * DAO Create method.
     * @param profile - Entity to store in DB.
     * @return - int value of generated Entity ID.
     */
    @Override
    public int create(final Profile profile) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, profile.getFirstName());
            statement.setString(2, profile.getLastName());
            statement.setString(3, profile.getAddress());
            statement.setString(4, profile.getPhone());
            statement.setInt(5, profile.getUserId());
            statement.executeUpdate();
            id = 0;
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param profile - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final Profile profile) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setString(1, profile.getFirstName());
            statement.setString(2, profile.getLastName());
            statement.setString(3, profile.getAddress());
            statement.setString(4, profile.getPhone());
            statement.setInt(5, profile.getUserId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param profile - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final Profile profile) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, profile.getUserId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of read Entities.
     */
    @Override
    public List<Profile> getAll() {
        List<Profile> profiles = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Profile profile = new Profile();
                    profile.setUserId(set.getInt("user_id"));
                    profile.setFirstName(set.getString("firstname"));
                    profile.setLastName(set.getString("lastname"));
                    profile.setAddress(set.getString("address"));
                    profile.setPhone(set.getString("phone"));
                    profiles.add(profile);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return profiles;
    }
}
