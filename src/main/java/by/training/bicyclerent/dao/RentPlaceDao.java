package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.RentPlace;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for CreditCard data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class RentPlaceDao implements Dao<RentPlace> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_BY_NAME = "SELECT"
            + " id FROM rentplace WHERE district = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " district FROM rentplace WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " id, district FROM rentplace";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT"
            + " INTO rentplace (district) VALUES (?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " rentplace SET district = ? WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE"
            + " FROM rentplace WHERE id = ?";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;
    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public RentPlaceDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO Read method.
     * @param id - entity ID value to read DB data by.
     * @return - read RentPlace Entity.
     */
    @Override
    public RentPlace read(final int id) {
        RentPlace rentPlace = new RentPlace();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    rentPlace.setId(id);
                    rentPlace.setDistrict(set.getString("district"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return rentPlace;
    }

    /**
     * Upgraded Read method.
     * @param district - String value to select Entities by.
     * @return - read RentPlace Entity.
     */
    public RentPlace readByName(final String district) {
        RentPlace rentPlace = new RentPlace();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_BY_NAME)) {
            statement.setString(1, district);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    rentPlace.setId(set.getInt("id"));
                    rentPlace.setDistrict(district);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return rentPlace;
    }

    /**
     * DAO Create method.
     * @param rentPlace - Entity to store in DB.
     * @return - int value of generated Entity ID.
     */
    @Override
    public int create(final RentPlace rentPlace) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, rentPlace.getDistrict());
            statement.executeUpdate();
            try (ResultSet set = statement.getGeneratedKeys()) {
                if (set.next()) {
                    id = set.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param rentPlace - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final RentPlace rentPlace) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setString(1, rentPlace.getDistrict());
            statement.setInt(2, rentPlace.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param rentPlace - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final RentPlace rentPlace) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, rentPlace.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of read Entities.
     */
    @Override
    public List<RentPlace> getAll() {
        List<RentPlace> places = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    RentPlace rentPlace = new RentPlace();
                    rentPlace.setId(set.getInt("id"));
                    rentPlace.setDistrict(set.getString("district"));
                    places.add(rentPlace);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return places;
    }
}
