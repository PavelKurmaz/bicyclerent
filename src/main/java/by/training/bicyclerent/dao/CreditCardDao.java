package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.CreditCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for CreditCard data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CreditCardDao implements Dao<CreditCard> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT"
            + " cardnumber, balance, overdraft FROM creditcard"
            + " WHERE user_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT"
            + " cardnumber, balance, overdraft, user_id FROM"
            + " creditcard";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO"
            + " creditcard (cardnumber, balance, overdraft,user_id)"
            + " VALUES (?, ?, ?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE"
            + " creditcard SET cardnumber = ?, balance = ?,"
            + " overdraft = ? WHERE user_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE FROM"
           + " creditcard WHERE user_id = ?";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;
    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public CreditCardDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO Read method.
     * @param id - entity ID value to read DB data by.
     * @return - read CreditCard Entity.
     */
    @Override
    public CreditCard read(final int id) {
        CreditCard creditCard = new CreditCard();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    creditCard.setUserId(id);
                    creditCard.setCardNumber(set.getString("cardnumber"));
                    creditCard.setBalance(BigDecimal.valueOf(
                            set.getDouble("balance")));
                    creditCard.setOverdraft(BigDecimal.valueOf(
                            set.getDouble("overdraft")));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return creditCard;
    }

    /**
     * DAO create method.
     * @param creditCard - CreditCard Entity to store in DB.
     * @return - int value of generated Entity ID.
     */
    @Override
    public int create(final CreditCard creditCard) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, creditCard.getCardNumber());
            statement.setDouble(2, creditCard.getBalance().doubleValue());
            statement.setDouble(3, creditCard.getOverdraft().doubleValue());
            statement.setInt(4, creditCard.getUserId());
            statement.executeUpdate();
            id = 0;
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param creditCard - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final CreditCard creditCard) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setString(1, creditCard.getCardNumber());
            statement.setDouble(2, creditCard.getBalance().doubleValue());
            statement.setDouble(3, creditCard.getOverdraft().doubleValue());
            statement.setInt(4, creditCard.getUserId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param creditCard - Entity to delete in DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final CreditCard creditCard) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, creditCard.getUserId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of read CreditCard Entities.
     */
    @Override
    public List<CreditCard> getAll() {
        List<CreditCard> cards = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    CreditCard creditCard = new CreditCard();
                    creditCard.setUserId(set.getInt("user_id"));
                    creditCard.setCardNumber(set.getString("cardnumber"));
                    creditCard.setBalance(BigDecimal.valueOf(
                            set.getDouble("balance")));
                    creditCard.setOverdraft(BigDecimal.valueOf(
                            set.getDouble("overdraft")));
                    cards.add(creditCard);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return cards;
    }
}
