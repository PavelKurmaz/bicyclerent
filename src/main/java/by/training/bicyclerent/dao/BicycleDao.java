package by.training.bicyclerent.dao;

import by.training.bicyclerent.entity.Bicycle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for Bicycle data access object.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class BicycleDao implements Dao<Bicycle> {
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_SQL = "SELECT rate, type_id,"
            + " rentplace_id, model FROM bicycle WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String READ_ALL = "SELECT id, rate,"
            + " type_id, rentplace_id, model FROM bicycle";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String INSERT_SQL = "INSERT INTO bicycle"
            + " (type_id, rentplace_id,  rate, model) VALUES (?, ?, ?, ?)";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String UPDATE_SQL = "UPDATE bicycle"
            + " SET type_id = ?, rentplace_id = ?, rate = ?, model = ?"
            + " WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String DELETE_SQL = "DELETE"
           + " FROM bicycle WHERE id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SELECT_SQL = "SELECT"
           + " id, rate, model FROM bicycle"
           + " WHERE type_id = ? AND rentplace_id = ?";
    /**
     * String value for PreparedStatement creation.
     */
    private static final String SELECT_RENTED = "SELECT DISTINCT"
            + " b.id, rate, type_id, rentplace_id, model FROM"
            + " bicycle as b JOIN `order` as o ON"
            + " b.id = o.bicycle_id AND o.status = 1";
    /**
     * Connection class object to conduct DB connection.
     */
    private Connection connection;

    /**
     * Public class constructor.
     * @param connection - Connection class object.
     */
    public BicycleDao(final Connection connection) {
        this.connection = connection;
    }

    /**
     * DAO Read method.
     * @param id - entity ID value to read DB data by.
     * @return - read Bicycle Entity.
     */
    @Override
    public Bicycle read(final int id) {
        Bicycle bicycle = new Bicycle();
        try (PreparedStatement statement =
                     connection.prepareStatement(READ_SQL)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                if (set.next()) {
                    bicycle.setId(id);
                    bicycle.setRate(BigDecimal.valueOf(set.getDouble("rate")));
                    bicycle.setTypeId(set.getInt("type_id"));
                    bicycle.setRentPlaceId(set.getInt("rentplace_id"));
                    bicycle.setModel(set.getString("model"));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return bicycle;
    }

    /**
     * Upgraded DAO ReadAll method.
     * @param typeId - int ID value for Bicycle`s Type.
     * @param placeId - int ID value for RentPlace.
     * @return - List of Entities filled with DB data.
     */
    public List<Bicycle> selectByTypeAndPlace(final int typeId,
                                              final int placeId) {
        List<Bicycle> bicycles = new ArrayList<>();
        try (PreparedStatement statement =
                     connection.prepareStatement(SELECT_SQL)) {
            statement.setInt(1, typeId);
            statement.setInt(2, placeId);
            statement.execute();
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Bicycle bicycle = new Bicycle();
                    bicycle.setId(set.getInt("id"));
                    bicycle.setTypeId(typeId);
                    bicycle.setRentPlaceId(placeId);
                    bicycle.setRate(BigDecimal.valueOf(set.getDouble("rate")));
                    bicycle.setModel(set.getString("model"));
                    bicycles.add(bicycle);
                }
                List<Bicycle> rented = getRented();
                bicycles.removeAll(rented);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return bicycles;
    }

    /**
     * DAO create method.
     * @param bicycle - Entity to put in DB.
     * @return - int value of created Entity`s ID.
     */
    @Override
    public int create(final Bicycle bicycle) {
        int id = -1;
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_SQL,
                             Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, bicycle.getTypeId());
            statement.setInt(2, bicycle.getRentPlaceId());
            statement.setBigDecimal(3, bicycle.getRate());
            statement.setString(4, bicycle.getModel());
            statement.executeUpdate();
            try (ResultSet set = statement.getGeneratedKeys()) {
                if (set.next()) {
                    id = set.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return id;
    }

    /**
     * DAO Update method.
     * @param bicycle - Entity to update in DB.
     * @return - boolean value signals if update was successful.
     */
    @Override
    public boolean update(final Bicycle bicycle) {
        try (PreparedStatement statement =
                     connection.prepareStatement(UPDATE_SQL)) {
            statement.setInt(1, bicycle.getTypeId());
            statement.setInt(2, bicycle.getRentPlaceId());
            statement.setBigDecimal(3, bicycle.getRate());
            statement.setString(4, bicycle.getModel());
            statement.setInt(5, bicycle.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO Delete method.
     * @param bicycle - Entity to delete from DB.
     * @return - boolean value signals if delete was successful.
     */
    @Override
    public boolean delete(final Bicycle bicycle) {
        try (PreparedStatement statement =
                     connection.prepareStatement(DELETE_SQL)) {
            statement.setInt(1, bicycle.getId());
            return (statement.executeUpdate() > 0);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    /**
     * DAO GetAll method.
     * @return - List of all read Entities.
     */
    @Override
    public List<Bicycle> getAll() {
        List<Bicycle> bicycles = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(READ_ALL);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Bicycle bicycle = new Bicycle();
                    bicycle.setId(set.getInt("id"));
                    bicycle.setTypeId(set.getInt("type_id"));
                    bicycle.setRentPlaceId(set.getInt("rentplace_id"));
                    bicycle.setRate(BigDecimal.valueOf(set.getDouble("rate")));
                    bicycle.setModel(set.getString("model"));
                    bicycles.add(bicycle);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return bicycles;
    }

    /**
     * Upgraded DAO read method to select rented Bicycles.
     * @return - List of read Bicycle Entities.
     */
    public List<Bicycle> getRented() {
        List<Bicycle> rented = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            statement.execute(SELECT_RENTED);
            try (ResultSet set = statement.getResultSet()) {
                while (set.next()) {
                    Bicycle bicycle = new Bicycle();
                    bicycle.setId(set.getInt("id"));
                    bicycle.setTypeId(set.getInt("type_id"));
                    bicycle.setRentPlaceId(set.getInt("rentplace_id"));
                    bicycle.setRate(BigDecimal.valueOf(set.getDouble("rate")));
                    bicycle.setModel(set.getString("model"));
                    rented.add(bicycle);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return rented;
    }

    /**
     * Upgraded DAO read method to select available Bicycles.
     * @return - List of read Bicycle Entities.
     */
    public List<Bicycle> getAvailable() {
        List<Bicycle> bicycles = getAll();
        List<Bicycle> rented = getRented();
        bicycles.removeAll(rented);
        return bicycles;
    }
}
