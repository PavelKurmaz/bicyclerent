package by.training.bicyclerent.dao;

import java.util.List;

/**
 * Interface to provide basic methods for DAO classes implementations.
 *
 * @param <T> - Generic class value.
 */
public interface Dao<T> {
    /**
     * Interface read method to implement.
     *
     * @param id - entity ID value to read DB data by.
     * @return - created Entity.
     */
    T read(int id);

    /**
     * Interface create method to implement.
     *
     * @param bean - Entity to store in DB.
     * @return - generated ID value after Entity creation.
     */
    int create(T bean);

    /**
     * Interface update method to implement.
     *
     * @param bean - Entity to update in DB.
     * @return - boolean value as update process result.
     */
    boolean update(T bean);

    /**
     * Interface delete method to implement.
     *
     * @param bean - Entity to delete from DB.
     * @return - boolean value as delete process result.
     */
    boolean delete(T bean);

    /**
     * Interface method to return all Entities of specified type.
     *
     * @return - List of read Entities.
     */
    List<T> getAll();
}
