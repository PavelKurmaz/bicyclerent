/**
 * Package contains Entity classes for BicycleRent Project.
 * Entities are used to store data in database.
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.entity;
