package by.training.bicyclerent.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class implements Application logic using Profile entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class Profile implements Serializable {
    /**
     * Private String firstName field.
     */
    private String firstName;
    /**
     * Private String lastName field.
     */
    private String lastName;
    /**
     * Private String address field.
     */
    private String address;
    /**
     * Private String phone field.
     */
    private String phone;
    /**
     * Private int userID field.
     * Used as foreign key.
     */
    private int userId;

    /**
     * Public empty class constructor.
     */
    public Profile() { //Bean Logic empty constructor.

    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Generated field setter method.
     *
     * @param firstName - field value to set.
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Generated field setter method.
     *
     * @param lastName - field value to set.
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Generated field setter method.
     *
     * @param address - field value to set.
     */
    public void setAddress(final String address) {
        this.address = address;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Generated field setter method.
     *
     * @param phone - field value to set.
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Generated field setter method.
     *
     * @param userId - field value to set.
     */
    public void setUserId(final int userId) {
        this.userId = userId;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Profile profile = (Profile) o;
        return userId == profile.userId
                && Objects.equals(firstName, profile.firstName)
                && Objects.equals(lastName, profile.lastName)
                && Objects.equals(address, profile.address)
                && Objects.equals(phone, profile.phone);
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, address, phone, userId);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "Profile{"
                + "firstName='" + firstName
                + '\''
                + ", lastName='"
                + lastName
                + '\''
                + ", address='"
                + address
                + '\''
                + ", phone='"
                + phone
                + '\''
                + ", userId="
                + userId
                + '}';
    }
}
