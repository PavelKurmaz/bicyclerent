package by.training.bicyclerent.entity;

/**
 * Enum class to hold User entity Status field values.
 */
public enum Status {
    /**
     * Enum String value.
     */
    REGULAR("regular"),
    /**
     * Enum String value.
     */
    WEAK("weak"),
    /**
     * Enum String value.
     */
    SILVER("silver"),
    /**
     * Enum String value.
     */
    GOLD("gold");
    /**
     * Private field String value.
     */
    private String value;

    /**
     * Class constructor.
     * @param type - String value for private field.
     */
    Status(final String type) {
        value = type;
    }

    /**
     * Generated field getter method.
     * @return - String field value.
     */
    public String getValue() {
        return value;
    }
}
