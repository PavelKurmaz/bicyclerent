package by.training.bicyclerent.entity;

import java.sql.Date;
import java.util.Objects;

/**
 * Class implements Application logic using Order entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class Order {
    /**
     * Private int entity ID field.
     */
    private int id;
    /**
     * Private int estimate renting time field.
     */
    private int estimate;
    /**
     * Private boolean entity status field.
     */
    private boolean status;
    /**
     * Private Date date field.
     */
    private Date date;
    /**
     * Private int userID field.
     * Used as foreign key.
     */
    private int userId;
    /**
     * Private int bicycleID field.
     * Used as foreign key.
     */
    private int bicycleId;
    /**
     * Private int RentPlace id field.
     * Used as foreign key.
     */
    private int start;
    /**
     * Private int RentPlace id field.
     * Used as foreign key.
     */
    private int finish;

    /**
     * Public empty class constructor.
     */
    public Order() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getId() {
        return id;
    }

    /**
     * Generated field setter method.
     *
     * @param id - field value to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getEstimate() {
        return estimate;
    }

    /**
     * Generated field setter method.
     *
     * @param estimate - field value to set.
     */
    public void setEstimate(final int estimate) {
        this.estimate = estimate;
    }

    /**
     * Generated field getter method.
     *
     * @return - boolean field value.
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Generated field setter method.
     *
     * @param status - field value to set.
     */
    public void setStatus(final boolean status) {
        this.status = status;
    }

    /**
     * Generated field getter method.
     *
     * @return - Date field value.
     */
    public Date getDate() {
        return new Date(date.getTime());
    }

    /**
     * Generated field setter method.
     *
     * @param date - field value to set.
     */
    public void setDate(final Date date) {
        this.date = new Date(date.getTime());
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Generated field setter method.
     *
     * @param userId - field value to set.
     */
    public void setUserId(final int userId) {
        this.userId = userId;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getBicycleId() {
        return bicycleId;
    }

    /**
     * Generated field setter method.
     *
     * @param bicycleId - field value to set.
     */
    public void setBicycleId(final int bicycleId) {
        this.bicycleId = bicycleId;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getStart() {
        return start;
    }

    /**
     * Generated field setter method.
     *
     * @param start - field value to set.
     */
    public void setStart(final int start) {
        this.start = start;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getFinish() {
        return finish;
    }

    /**
     * Generated field setter method.
     *
     * @param finish - field value to set.
     */
    public void setFinish(final int finish) {
        this.finish = finish;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Order order = (Order) o;
        return id == order.id
                && estimate == order.estimate
                && status == order.status
                && userId == order.userId
                && bicycleId == order.bicycleId
                && start == order.start
                && finish == order.finish;
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, estimate, status,
                date, userId, bicycleId, start, finish);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "Order{"
                + "id="
                + id
                + ", estimate="
                + estimate
                + ", status="
                + status
                + ", date="
                + date
                + ", userId="
                + userId
                + ", bicycleId="
                + bicycleId
                + ", start="
                + start
                + ", finish="
                + finish
                + '}';
    }
}
