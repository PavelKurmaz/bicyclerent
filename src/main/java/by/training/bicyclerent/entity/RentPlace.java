package by.training.bicyclerent.entity;

import java.util.Objects;

/**
 * Class implements Application logic using RentPlace entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class RentPlace {
    /**
     * Private int entity ID field.
     */
    private int id;
    /**
     * Private String district field.
     */
    private String district;

    /**
     * Public empty class constructor.
     */
    public RentPlace() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getId() {
        return id;
    }

    /**
     * Generated field setter method.
     *
     * @param id - field value to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getDistrict() {
        return district;
    }

    /**
     * Generated field setter method.
     *
     * @param district - field value to set.
     */
    public void setDistrict(final String district) {
        this.district = district;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RentPlace rentPlace = (RentPlace) o;
        return id == rentPlace.id
                && Objects.equals(district, rentPlace.district);
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, district);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "RentPlace{"
                + "id="
                + id
                + ", district='"
                + district
                + '\''
                + '}';
    }
}
