package by.training.bicyclerent.entity;

import java.util.Objects;

/**
 * Class implements Application logic using Type entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class Type {
    /**
     * Private int entity ID field.
     */
    private int id;
    /**
     * Private String description field.
     */
    private String description;
    /**
     * Private String path field.
     */
    private String path;

    /**
     * Public empty class constructor.
     */
    public Type() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getPath() {
        return path;
    }

    /**
     * Generated field setter method.
     *
     * @param path - field value to set.
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getId() {
        return id;
    }

    /**
     * Generated field setter method.
     *
     * @param id - field value to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Generated field setter method.
     *
     * @param description - field value to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Type type = (Type) o;
        return id == type.id
                && Objects.equals(description, type.description);
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "Type{"
                + "id="
                + id
                + ", description='"
                + description
                + '\''
                + '}';
    }
}
