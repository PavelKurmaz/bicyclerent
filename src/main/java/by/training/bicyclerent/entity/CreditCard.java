package by.training.bicyclerent.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class implements Application logic using CreditCard entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CreditCard {
    /**
     * Private String cardNumber field.
     */
    private String cardNumber;
    /**
     * Private BigDecimal balance field.
     */
    private BigDecimal balance;
    /**
     * Private BigDecimal overdraft field.
     */
    private BigDecimal overdraft;
    /**
     * Private int userID field.
     * Used as foreign key.
     */
    private int userId;

    /**
     * Public class constructor.
     */
    public CreditCard() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Generated field setter method.
     *
     * @param cardNumber - field value to set.
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Generated field getter method.
     *
     * @return - BigDecimal field value.
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Generated field setter method.
     *
     * @param balance - field value to set.
     */
    public void setBalance(final BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * Generated field getter method.
     *
     * @return - BigDecimal field value.
     */
    public BigDecimal getOverdraft() {
        return overdraft;
    }

    /**
     * Generated field setter method.
     *
     * @param overdraft - field value to set.
     */
    public void setOverdraft(final BigDecimal overdraft) {
        this.overdraft = overdraft;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Generated field setter method.
     *
     * @param userId - field value to set.
     */
    public void setUserId(final int userId) {
        this.userId = userId;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreditCard that = (CreditCard) o;
        return userId == that.userId
                && Objects.equals(cardNumber, that.cardNumber)
                && Objects.equals(balance, that.balance)
                && Objects.equals(overdraft, that.overdraft);
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, balance, overdraft, userId);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "CreditCard{"
                + "cardNumber='"
                + cardNumber
                + '\''
                + ", balance="
                + balance
                + ", overdraft="
                + overdraft
                + ", userId="
                + userId
                + '}';
    }
}
