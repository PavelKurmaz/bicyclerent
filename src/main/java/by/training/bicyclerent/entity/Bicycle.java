package by.training.bicyclerent.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class implements Application logic using Bicycle entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class Bicycle {
    /**
     * Private entity ID field.
     */
    private int id;
    /**
     * Private BigDecimal renting rate field.
     */
    private BigDecimal rate;
    /**
     * Private Bicycle`s Type id field.
     * Used as foreign key.
     */
    private int typeId;
    /**
     * Private Bicycle`s RentPlace id field.
     * Used as foreign key.
     */
    private int rentPlaceId;
    /**
     * Private Bicycle`s Model description field.
     */
    private String model;

    /**
     * Public empty constructor.
     */
    public Bicycle() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getId() {
        return id;
    }

    /**
     * Generated field setter method.
     *
     * @param id - field value to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Generated field getter method.
     *
     * @return - BigDecimal field value.
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Generated field setter method.
     *
     * @param rate - field value to set.
     */
    public void setRate(final BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     * Generated field setter method.
     *
     * @param typeId - field value to set.
     */
    public void setTypeId(final int typeId) {
        this.typeId = typeId;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getRentPlaceId() {
        return rentPlaceId;
    }

    /**
     * Generated field setter method.
     *
     * @param rentPlaceId - field value to set.
     */
    public void setRentPlaceId(final int rentPlaceId) {
        this.rentPlaceId = rentPlaceId;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getModel() {
        return model;
    }

    /**
     * Generated field setter method.
     *
     * @param model - field value to set.
     */
    public void setModel(final String model) {
        this.model = model;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bicycle bicycle = (Bicycle) o;
        return id == bicycle.id
                && typeId == bicycle.typeId
                && rentPlaceId == bicycle.rentPlaceId
                && Objects.equals(rate, bicycle.rate)
                && Objects.equals(model, bicycle.model);
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, rate, typeId, rentPlaceId, model);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "Bicycle{"
                + "id="
                + id
                + ", rate="
                + rate
                + ", typeId="
                + typeId
                + ", rentPlaceId="
                + rentPlaceId
                + ", model='"
                + model
                + '\''
                + '}';
    }
}
