package by.training.bicyclerent.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class implements Application logic using User entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class User implements Serializable{
    /**
     * Private int entity ID field.
     */
    private int id;
    /**
     * Private String login field.
     */
    private String login;
    /**
     * Private String password field.
     */
    private String password;
    /**
     * Private String email field.
     */
    private String email;
    /**
     * Private Status field.
     */
    private Status status;
    /**
     * Private boolean blocked field.
     */
    private boolean blocked;
    /**
     * Private boolean disabled field.
     */
    private boolean disabled;
    /**
     * Private int role ID field.
     */
    private int role;
    /**
     * Private String path field.
     */
    private String path;

    /**
     * Public empty class constructor.
     */
    public User() { //Bean Logic empty constructor.
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getPath() {
        return path;
    }

    /**
     * Generated field setter method.
     *
     * @param path - field value to set.
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getId() {
        return id;
    }

    /**
     * Generated field setter method.
     *
     * @param id - field value to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Generated field setter method.
     *
     * @param login - field value to set.
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Generated field setter method.
     *
     * @param password - field value to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Generated field getter method.
     *
     * @return - String field value.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Generated field setter method.
     *
     * @param email - field value to set.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Generated field getter method.
     *
     * @return - Status field value.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Generated field setter method.
     *
     * @param status - field value to set.
     */
    public void setStatus(final Status status) {
        this.status = status;
    }

    /**
     * Generated field getter method.
     *
     * @return - boolean field value.
     */
    public boolean isBlocked() {
        return blocked;
    }

    /**
     * Generated field setter method.
     *
     * @param blocked - field value to set.
     */
    public void setBlocked(final boolean blocked) {
        this.blocked = blocked;
    }

    /**
     * Generated field getter method.
     *
     * @return - int field value.
     */
    public int getRole() {
        return role;
    }

    /**
     * Generated field setter method.
     *
     * @param role - field value to set.
     */
    public void setRole(final int role) {
        this.role = role;
    }

    /**
     * Generated field getter method.
     *
     * @return - boolean field value.
     */
    public boolean isDisabled() {
        return disabled;
    }

    /**
     * Generated field setter method.
     *
     * @param disabled - field value to set.
     */
    public void setDisabled(final boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * Overridden equals method.
     *
     * @param o - Object to check equality.
     * @return - boolean equality value.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return id == user.id
                && Objects.equals(login, user.login)
                && Objects.equals(password, user.password)
                && Objects.equals(email, user.email)
                && status == user.status;
    }

    /**
     * Overridden hashCode method.
     *
     * @return - int value of calculated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, email, status);
    }

    /**
     * Overridden toString() method.
     *
     * @return - String value of object.
     */
    @Override
    public String toString() {
        return "User{"
                + "id="
                + id
                + ", login='"
                + login
                + '\''
                + ", password='"
                + password
                + '\''
                + ", email='"
                + email
                + '\''
                + ", status="
                + status
                + '}';
    }
}
