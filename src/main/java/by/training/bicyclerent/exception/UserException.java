package by.training.bicyclerent.exception;
/**
 * Application Exception class used for handling logic errors.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class UserException extends Exception {
    /**
     * Private String field to contain error message.
     */
    private String errorMessage;
    /**
     * Public class constructor.
     * @param message - Error message to store.
     */
    public UserException(final String message) {
        errorMessage = message;
    }
    /**
     * Getter method for errorMessage field.
     * @return - String value to return.
     */
    @Override
    public String getMessage() {
        return errorMessage;
    }
}
