package by.training.bicyclerent.exception;

/**
 * Application Exception class used for handling errors.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class AppException extends Exception {
    /**
     * Private String field to contain error message.
     */
    private String errorMessage;

    /**
     * Public class constructor.
     * @param message - Error message to store.
     */
    public AppException(final String message) {
        errorMessage = message;
    }

    /**
     * Public empty constructor.
     */
    public AppException() { } //Empty default constructor.

    /**
     * Getter method for errorMessage field.
     * @return - String value to return.
     */
    @Override
    public String getMessage() {
        return errorMessage;
    }
}
