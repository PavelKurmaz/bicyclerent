/**
 * Package contains main Servlet class for BicycleRent Project.
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.servlet;
