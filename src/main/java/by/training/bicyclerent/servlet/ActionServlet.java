package by.training.bicyclerent.servlet;

import by.training.bicyclerent.action.ActionFactory;
import by.training.bicyclerent.action.Actions;
import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.exception.UserException;
import by.training.bicyclerent.util.Generator;
import by.training.bicyclerent.exception.AppException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet class to process Http Requests.
 * Extends HttpServlet class.
 *
 * @author Pavel Kurmaz.
 * @version 1.0
 */
@WebServlet("/action")
@MultipartConfig
public class ActionServlet extends HttpServlet {
    /**
     * Log4j2 Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * ActionFactory instance to process HttpRequests.
     */
    private static final ActionFactory FACTORY = new ActionFactory();
    /**
     * ServletContext instance object.
     */
    private ServletContext servletContext;

    /**
     * Public class constructor.
     */
    public ActionServlet() {
        super();
    }

    /**
     * Overridden initialisation method for Servlet.
     */
    @Override
    public void init() {
        servletContext = getServletContext();
        String dbUserName = servletContext.getInitParameter("dbusername");
        String dbUserPassword = servletContext
                .getInitParameter("dbuserpassword");
        String path = servletContext.getInitParameter("propertiespath");
        Generator.generateDbAdmin(dbUserName, dbUserPassword, path);
        if (!Generator.generateAdmin(
                servletContext.getInitParameter("adminlogin"),
                servletContext.getInitParameter("adminpassword"),
                servletContext.getInitParameter("adminemail")
        )) {
            LOG.error("database admin creation failed!");
        }
    }

    /**
     * Main GET request processing method.
     *
     * @param request  - HttpServletRequest.
     * @param response - HttpServletResponse.
     */
    @Override
    protected void doGet(final HttpServletRequest request,
                         final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Actions action = FACTORY.getAction(request);
            String nextPage = action.command.executeGet(request, response);
            if (nextPage == null) {
                response.sendRedirect(
                        "action?command=" + action.toString().toLowerCase());
            } else {
                final RequestDispatcher requestDispatcher =
                        servletContext.getRequestDispatcher(
                                "/jsp/" + nextPage + ".jsp");
                requestDispatcher.forward(request, response);
            }
        } catch (AppException e) {
            request.setAttribute("errmessage", e.toString());
            StringBuilder sb = new StringBuilder();
            StackTraceElement[] stackTrace = e.getStackTrace();
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(stackTraceElement).append("<br>");
                if (stackTraceElement.toString().contains("ActionServlet")) {
                    break;
                }
            }
            request.setAttribute("stack", sb.toString());
            RequestDispatcher requestDispatcher =
                    servletContext.getRequestDispatcher(Actions.ERROR.jsp);
            requestDispatcher.forward(request, response);
        } catch (UserException e) {
            request.setAttribute("errmessage", e.getMessage());
            RequestDispatcher requestDispatcher =
                    servletContext.getRequestDispatcher("/jsp/usererror.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    /**
     * Main POST request processing method.
     *
     * @param request  - HttpServletRequest.
     * @param response - HttpServletResponse.
     */
    @Override
    protected void doPost(final HttpServletRequest request,
                          final HttpServletResponse response)
            throws ServletException, IOException {

        try {
            final Actions action = FACTORY.getAction(request);
            String nextPage = action.command.executePost(request, response);
            if (nextPage == null) {
                response.sendRedirect(
                        "action?command=" + action.toString().toLowerCase());
            } else {
                final RequestDispatcher requestDispatcher =
                        servletContext.getRequestDispatcher(
                                "/jsp/" + nextPage + ".jsp");
                requestDispatcher.forward(request, response);
            }
        } catch (AppException e) {
            request.setAttribute("errmessage", e.getMessage());
            StringBuilder sb = new StringBuilder();
            StackTraceElement[] stackTrace = e.getStackTrace();
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(stackTraceElement).append("<br>");
                if (stackTraceElement.toString().contains("ActionServlet")) {
                    break;
                }
            }
            request.setAttribute("stack", sb.toString());
            RequestDispatcher requestDispatcher =
                    servletContext.getRequestDispatcher(Actions.ERROR.jsp);
            requestDispatcher.forward(request, response);
        } catch (UserException e) {
            request.setAttribute("errmessage", e.getMessage());
            RequestDispatcher requestDispatcher =
                    servletContext.getRequestDispatcher(Actions.USERERROR.jsp);
            requestDispatcher.forward(request, response);
        }
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().closeConnections();
    }
}
