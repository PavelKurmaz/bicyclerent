package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.OrderDao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.entity.Bicycle;
import by.training.bicyclerent.entity.Order;
import by.training.bicyclerent.entity.RentPlace;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;
/**
 * Command class to implement Application logic.
 * Holds methods for calculating RentPlaces summary income.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdSummary extends Command {
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<RentPlace> placeDao = new RentPlaceDao(connection);
        List<RentPlace> places = placeDao.getAll();
        request.setAttribute("places", places);
        ConnectionPool.getInstance().freeConnection(connection);
        return "presummary";
    }
    /**
     * Post method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        OrderDao orderDao = new OrderDao(connection);
        Dao<Bicycle> bicycleDao = new BicycleDao(connection);
        List<Bicycle> bicycles = bicycleDao.getAll();
        if (request.getParameter("summary") != null) {
            List<Order> orders = orderDao.getAll();
            request.setAttribute("orders", orders);
            request.setAttribute("bicycles", bicycles);
            request.setAttribute("rentplace", "All");
            request.setAttribute("summary", orderDao.getSum());
            ConnectionPool.getInstance().freeConnection(connection);
            return "ordersummary";
        } else {
            int placeId = Integer.parseInt(request.getParameter("place"));
            Dao<RentPlace> rentPlaceDao = new RentPlaceDao(connection);
            String district = rentPlaceDao.read(placeId).getDistrict();
            List<Order> orders = orderDao.getByPlaceId(placeId);
            request.setAttribute("orders", orders);
            request.setAttribute("bicycles", bicycles);
            request.setAttribute("rentplace", district);
            request.setAttribute("summary", orderDao.getDistrictSum(placeId));
            ConnectionPool.getInstance().freeConnection(connection);
            return "ordersummary";
        }
    }
}
