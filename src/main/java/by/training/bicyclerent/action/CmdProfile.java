package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.ProfileDao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Profile;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;
import by.training.bicyclerent.util.Generator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.Random;

/**
 * Command class to implement Application logic.
 * Allows operations with Profile Entities.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdProfile extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        User user = (User) request.getSession().getAttribute("user");
        int id = user.getId();
        Dao<User> userDao = new UserDao(connection);
        Dao<Profile> profileDao = new ProfileDao(connection);
        if (userDao.read(id) != null) {
            Profile profile = profileDao.read(id);
            request.getSession().setAttribute("profile", profile);
        } else {
            throw new AppException("No such user, or data corrupted");
        }
        return "profile";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Profile profile = new Profile();
        profile.setFirstName(Validator.getString(request, "firstName"));
        profile.setLastName(Validator.getString(request, "lastName"));
        profile.setAddress(Validator.getString(request, "address"));
        profile.setPhone(Validator.getString(request, "phone"));
        User user = (User) request.getSession().getAttribute("user");
        profile.setUserId(user.getId());
        try (InputStream content = request.getPart("image").getInputStream()) {
            if (content.available() != 0) {
                String filename = Generator.generateRandomInt(1000)
                        + request.getPart("image").getSubmittedFileName();
                String filePath = request.getServletContext()
                        .getInitParameter("useravatarpath");
                File file = new File(filePath + File.separator + filename);
                file.getParentFile().mkdirs();
                Path path = file.toPath();
                while (Files.isReadable(path)) {
                    file = new File(filePath
                            + Generator.generateRandomInt(10)
                            + filename);
                    path = file.toPath();
                }
                Files.copy(content, path);
                user.setPath("img/avatar/" + filename);
            } else {
                user.setPath("img/avatar/random.jpg");
            }
            Dao<User> userDao = new UserDao(connection);
            userDao.update(user);
        } catch (IOException | ServletException e) {
            throw new AppException("Error reading image!");
        }
        Dao<Profile> dao = new ProfileDao(connection);
        if (dao.read(user.getId()).getFirstName() != null) {
            if (!dao.update(profile)) {
                throw new UserException("Failed to update user profile!");
            }
        } else {
            if (dao.create(profile) < 0) {
                throw new UserException("Failed to create user profile!");
            }
        }
        request.getSession().setAttribute("profile", profile);
        ConnectionPool.getInstance().freeConnection(connection);
        return "index";
    }
}
