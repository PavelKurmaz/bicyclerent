package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.Paginator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

/**
 * Command class to implement Application logic.
 * Allows editing of all User Entities.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
class CmdEditUsers extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response) {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<User> dao = new UserDao(connection);
        List<User> users = dao.getAll();
        int pageNumber = Paginator.getPageNumber(request, users.size());
        int startPosition = Paginator.getStartPosition(pageNumber);
        int finishPosition = Paginator.getFinishPosition(users.size(),
                pageNumber + 1);
        users = users.subList(startPosition, finishPosition);
        request.setAttribute("users", users);
        ConnectionPool.getInstance().freeConnection(connection);
        return "editusers";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        int id = Validator.getInteger(request, "id");
        String login = Validator.getString(request, "login");
        String email = Validator.getEmail(request);
        String password = Validator.getString(request, "password");
        String status = Validator.getString(request, "status");
        String blocked = request.getParameter("blocked");
        String disabled = request.getParameter("disabled");
        Dao<User> dao = new UserDao(connection);
        User user = new User();
        user.setId(id);
        user.setRole(0);
        user.setEmail(email);
        user.setLogin(login);
        user.setStatus(Status.valueOf(status.toUpperCase()));
        user.setPassword(password);
        user.setBlocked(blocked != null);
        user.setDisabled(disabled != null);
        if (request.getParameter("Update") != null) {
            dao.update(user);
        } else if (request.getParameter("Delete") != null) {
            dao.delete(user);
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return null;
    }
}

