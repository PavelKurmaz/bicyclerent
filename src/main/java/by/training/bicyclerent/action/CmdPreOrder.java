package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.Bicycle;
import by.training.bicyclerent.entity.RentPlace;
import by.training.bicyclerent.entity.Type;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.Paginator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
/**
 * Command class to implement Application logic.
 * Holds preliminary procedures for CreateOrder command.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdPreOrder extends Command {
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<RentPlace> rentPlaceDao = new RentPlaceDao(connection);
        Dao<Type> typeDao = new TypeDao(connection);
        List<RentPlace> places;
        List<Type> types;
        try {
            connection.setAutoCommit(false);
            places = rentPlaceDao.getAll();
            types = typeDao.getAll();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new AppException(e.getMessage());
        }
        request.setAttribute("places", places);
        request.setAttribute("types", types);
        ConnectionPool.getInstance().freeConnection(connection);
        return "preorder";
    }
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        BicycleDao dao = new BicycleDao(connection);
        Dao<RentPlace> rentPlaceDao = new RentPlaceDao(connection);
        Dao<Type> typeDao = new TypeDao(connection);
        List<Bicycle> bicycles;
        List<RentPlace> places;
        RentPlace place;
        Type type;
        try {
            connection.setAutoCommit(false);
            int typeId = Validator.getInteger(request, "type");
            int placeId = Validator.getInteger(request, "place");
            bicycles = dao.selectByTypeAndPlace(typeId, placeId);
            int pageNumber = Paginator.getPageNumber(request, bicycles.size());
            int startPosition = Paginator.getStartPosition(pageNumber);
            int finishPosition = Paginator.getFinishPosition(bicycles.size(),
                    pageNumber + 1);
            bicycles = bicycles.subList(startPosition, finishPosition);
            place = rentPlaceDao.read(placeId);
            type = typeDao.read(typeId);
            places = rentPlaceDao.getAll();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new AppException(e.getMessage());
        }
        request.setAttribute("type", type);
        request.setAttribute("place", place);
        request.setAttribute("bicycles", bicycles);
        request.setAttribute("places", places);
        ConnectionPool.getInstance().freeConnection(connection);
        return "createorder";
    }

}
