package by.training.bicyclerent.action;

import by.training.bicyclerent.exception.AppException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command class to implement Application logic.
 * Creates and adds new RentPlace Entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdAddRentPlace extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        return "addrentplace";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        return null;
    }
}
