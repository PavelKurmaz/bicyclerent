package by.training.bicyclerent.action;

/**
 * Enum class that determines Administrator commands.
 * Security reasons.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public enum AdminAction {
    /**
     * Enum command name value.
     */
    EDITUSERS,
    /**
     * Enum command name value.
     */
    ADDBICYCLE,
    /**
     * Enum command name value.
     */
    ADDRENTPLACE,
    /**
     * Enum command name value.
     */
    ADDTYPE,
    /**
     * Enum command name value.
     */
    EDITTYPES,
    /**
     * Enum command name value.
     */
    EDITRENTPLACES,
    /**
     * Enum command name value.
     */
    EDITBICYCLES,
    /**
     * Enum command name value.
     */
    ERROR,
    /**
     * Enum command name value.
     */
    SUMMARY
}
