package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.Bicycle;
import by.training.bicyclerent.entity.RentPlace;
import by.training.bicyclerent.entity.Type;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.Paginator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Command class to implement Application logic.
 * Allows editing of all available Bicycle Entities.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdEditBicycles extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        BicycleDao bicycleDao = new BicycleDao(connection);
        Dao<RentPlace> rentPlaceDao = new RentPlaceDao(connection);
        Dao<Type> typeDao = new TypeDao(connection);
        List<Bicycle> bicycles;
        List<RentPlace> rentPlaces;
        List<Type> types;
        List<Bicycle> rented;
        try {
            connection.setAutoCommit(false);
            bicycles = bicycleDao.getAvailable();
            rented = bicycleDao.getRented();
            rentPlaces = rentPlaceDao.getAll();
            types = typeDao.getAll();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new AppException(e.getMessage());
        }
        int pageNumber = Paginator.getPageNumber(request, bicycles.size());
        int startPosition = Paginator.getStartPosition(pageNumber);
        int finishPosition = Paginator.getFinishPosition(bicycles.size(),
                pageNumber + 1);
        bicycles = bicycles.subList(startPosition, finishPosition);
        request.setAttribute("bicycles", bicycles);
        request.setAttribute("rentplaces", rentPlaces);
        request.setAttribute("types", types);
        request.setAttribute("rented", rented);
        ConnectionPool.getInstance().freeConnection(connection);
        return "editbicycles";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<Bicycle> dao = new BicycleDao(connection);
        if (request.getParameter("Update") != null) {
            int id = Validator.getInteger(request, "id");
            Bicycle bicycle = dao.read(id);
            BigDecimal rate = Validator.getDouble(request, "rate");
            String model = Validator.getString(request, "model");
            int rentPlaceId = Validator.getInteger(request, "rentplace");
            int typeId = Validator.getInteger(request, "type");
            bicycle.setTypeId(typeId);
            bicycle.setRentPlaceId(rentPlaceId);
            bicycle.setModel(model);
            bicycle.setRate(rate);
            if (!dao.update(bicycle)) {
                throw new AppException("Failed to update bicycle!");
            }
        } else if (request.getParameter("Delete") != null) {
            int id = Validator.getInteger(request, "id");
            Bicycle bicycle = dao.read(id);
            if (!dao.delete(bicycle)) {
                throw new AppException("Failed to delete bicycle!");
            }
        } else if (request.getParameter("Add") != null) {
            int typeId = Validator.getInteger(request, "type");
            int rentPlaceId = Validator.getInteger(request, "rentplace");
            BigDecimal rate = Validator.getDouble(request, "rate");
            String model = Validator.getString(request, "model");
            Bicycle bicycle = new Bicycle();
            bicycle.setModel(model);
            bicycle.setRate(rate);
            bicycle.setTypeId(typeId);
            bicycle.setRentPlaceId(rentPlaceId);
            int id = dao.create(bicycle);
            if (id != -1) {
                bicycle.setId(id);
            } else {
                throw new AppException("Failed to create bicycle!");
            }
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return null;
    }

}
