package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.ProfileDao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Profile;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

/**
 * Command class to implement Application logic.
 * Realizes login procedure.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
class CmdLogin extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            session.removeAttribute("user");
            session.removeAttribute("profile");
        }
        return "login";
    }

    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        String login = Validator.getString(request, "login");
        String password = Validator.getString(request, "password");
        UserDao dao = new UserDao(connection);
        User user = dao.readByName(login);
        if (user.getPassword().equals(password)) {
            if (user.isBlocked() || user.isDisabled()) {
                throw new UserException(
                        "Sorry, your account has been blocked or disabled!");
            }
            Dao<Profile> profileDao = new ProfileDao(connection);
            Profile profile = profileDao.read(user.getId());
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("profile", profile);
            ConnectionPool.getInstance().freeConnection(connection);
            return "index";
        } else {
            throw new UserException("Wrong username or password");
        }
    }
}
