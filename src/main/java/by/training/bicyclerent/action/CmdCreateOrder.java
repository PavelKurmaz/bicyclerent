package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.CreditCardDao;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.OrderDao;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.entity.Order;
import by.training.bicyclerent.entity.Bicycle;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.CreditCard;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.CashUtils;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;

/**
 * Command class to implement Application logic.
 * Creates a new Order for Users and implements balance and credit logic.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdCreateOrder extends Command {

    /**
     * Static constant for Error Message.
     */
    private static final String ERROR_MESSAGE = "errormessage";
    /**
     * Static constant for Failed Order path to redirect.
     */
    private static final String ORDER_FAILED = "orderfailed";

    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        return null;
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            return "login";
        }
        if (user.getStatus().equals(Status.WEAK)) {
            request.setAttribute(ERROR_MESSAGE,
                    "Your status doesn't allow new orders!");
            return ORDER_FAILED;
        }
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<Order> orderDao = new OrderDao(connection);
        Dao<Bicycle> bicycleDao = new BicycleDao(connection);
        Dao<CreditCard> creditCardDao = new CreditCardDao(connection);
        int estimate = Validator.getInteger(request, "estimate");
        int userId = user.getId();
        int bicycleId = Validator.getInteger(request, "bicycleId");
        int start = Validator.getInteger(request, "start");
        int finish = Validator.getInteger(request, "finish");
        Bicycle bicycle = bicycleDao.read(bicycleId);
        BigDecimal cost = BigDecimal.valueOf(bicycle.getRate()
                .doubleValue() * estimate);
        CreditCard creditCard = creditCardDao.read(userId);
        if (creditCard.getBalance() == null) {
            request.setAttribute(ERROR_MESSAGE,
                     "Sorry, you have no valid credit card!");
            return ORDER_FAILED;
        }
        if (!CashUtils.checkBalance(creditCard, cost)) {
            request.setAttribute(ERROR_MESSAGE,
                    "You don't have enough funds!");
            return ORDER_FAILED;
        }
        Order order = new Order();
        order.setFinish(finish);
        order.setStart(start);
        order.setStatus(true);
        order.setUserId(userId);
        order.setBicycleId(bicycleId);
        order.setDate(new Date(System.currentTimeMillis()));
        order.setEstimate(estimate);
        int id = orderDao.create(order);
        if (id != -1) {
            order.setId(id);
            request.setAttribute("order", order);
            request.setAttribute("bicycle", bicycle);
            request.setAttribute("cost", cost);
            ConnectionPool.getInstance().freeConnection(connection);
            return "orderconfirmed";
        } else {
            ConnectionPool.getInstance().freeConnection(connection);
            request.setAttribute(ERROR_MESSAGE,
                    "Order creation failed, contact support.");
            return ORDER_FAILED;
        }
    }
}
