package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
/**
 * Command class to implement Application logic.
 * Holds methods for signup procedure.
 * @author Pavel Kurmaz
 * @version 1.0
 */
class CmdSignUp extends Command {
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        UserDao dao = new UserDao(connection);
        String login = Validator.getString(request, "login");
        if (dao.readByName(login).getLogin() != null) {
            throw new UserException("User with login "
                    + login + " already exists!");
        }
        String email = Validator.getEmail(request);
        String password = Validator.getString(request, "password");
        Status status = Status.REGULAR;
        User user = new User();
        user.setBlocked(false);
        user.setStatus(status);
        user.setEmail(email);
        user.setPassword(password);
        user.setLogin(login);
        user.setDisabled(false);
        user.setPath("img/avatar/random.jpg");
        int id = dao.create(user);
        if (id != -1) {
            user.setId(id);
            request.getSession().setAttribute("user", user);
            ConnectionPool.getInstance().freeConnection(connection);
            return "profile";
        } else {
            throw new UserException(
                    "Failed to create user! Contact tech support");
        }
    }
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response) {
        return "signup";
    }
}
