package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.*;
import by.training.bicyclerent.entity.Bicycle;
import by.training.bicyclerent.entity.CreditCard;
import by.training.bicyclerent.entity.Order;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.CashUtils;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Command class to implement Application logic.
 * Holds methods for viewing user`s orders procedure.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdViewUserOrders extends Command {

    private static final String USER_STATUS_MESSAGE = "userStatusMessage";
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        OrderDao dao = new OrderDao(connection);
        Dao<Bicycle> bicycleDao = new BicycleDao(connection);
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            return "login";
        }
        List<Order> orders = dao.getByUserId(user.getId());
        List<Bicycle> bicycles = new LinkedList<>();
        for (Order order : orders) {
            Bicycle bicycle = bicycleDao.read(order.getBicycleId());
            bicycles.add(bicycle);
        }
        request.setAttribute("bicycles", bicycles);
        request.setAttribute("orders", orders);
        String statusMessage = (String) request.getSession().getAttribute(USER_STATUS_MESSAGE);
        if (statusMessage != null) {
            request.setAttribute(USER_STATUS_MESSAGE, statusMessage);
            request.getSession().removeAttribute(USER_STATUS_MESSAGE);
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return "viewuserorders";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        OrderDao dao = new OrderDao(connection);
        Dao<Bicycle> bicycleDao = new BicycleDao(connection);
        Dao<CreditCard> creditCardDao = new CreditCardDao(connection);
        try {
            connection.setAutoCommit(false);
            int orderId = Validator.getInteger(request, "id");
            Order order = dao.read(orderId);
            Bicycle bicycle = bicycleDao.read(order.getBicycleId());
            CreditCard creditCard = creditCardDao.read(order.getUserId());
            BigDecimal cost = BigDecimal.valueOf(bicycle.getRate()
                    .doubleValue() * order.getEstimate());
            CashUtils.proceedPayment(creditCard, cost);
            if (creditCardDao.update(creditCard)) {
                order.setStatus(false);
            }
            bicycle.setRentPlaceId(order.getFinish());
            dao.update(order);
            bicycleDao.update(bicycle);
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new AppException(e.getMessage());
        }
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            return "login";
        }
        List<Order> orders = dao.getByUserId(user.getId());
        if (CashUtils.upgradeUser(user, orders)) {
            Dao<User> userDao = new UserDao(connection);
            userDao.update(user);
            request.getSession().setAttribute(USER_STATUS_MESSAGE,
                    "Your Status upgraded to " + user.getStatus());
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return null;
    }
}
