package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.CreditCardDao;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.entity.CreditCard;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Connection;

/**
 * Command class to implement Application logic.
 * Realizes all operations with CreditCard Entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdCard extends Command {
    /**
     * Static constant field that holds default starting balance value.
     */
    private static final BigDecimal START_BALANCE = BigDecimal.valueOf(100);

    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            return "login";
        }
        Dao<CreditCard> dao = new CreditCardDao(connection);
        CreditCard creditCard = dao.read(user.getId());
        String cardNumber = creditCard.getCardNumber();
        if (cardNumber != null) {
            creditCard.setCardNumber(cardNumber.replaceFirst(
                    "[0-9]{12}", "************"));
        }
        request.setAttribute("creditcard", creditCard);
        return "creditcard";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException, UserException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            return "login";
        }
        String partOne = Validator.getString(request, "one");
        String partTwo = Validator.getString(request, "two");
        String partThree = Validator.getString(request, "three");
        String partFour = Validator.getString(request, "four");
        String cardNumber = partOne + partTwo + partThree + partFour;
        Dao<CreditCard> dao = new CreditCardDao(connection);
        CreditCard creditCard = new CreditCard();
        creditCard.setUserId(user.getId());
        creditCard.setCardNumber(cardNumber);
        creditCard.setBalance(START_BALANCE);
        creditCard.setOverdraft(BigDecimal.ZERO);
        if (dao.create(creditCard) < 0) {
            ConnectionPool.getInstance().freeConnection(connection);
            throw new UserException("Error adding creditcard");
        } else {
            ConnectionPool.getInstance().freeConnection(connection);
            return null;
        }
    }
}
