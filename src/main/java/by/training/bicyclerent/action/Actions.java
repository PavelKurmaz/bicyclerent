package by.training.bicyclerent.action;

/**
 * Enum class to hold and generate Command classes.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public enum Actions {
    /**
     * Actions Enum value.
     */
    INDEX {
        {
            command = new CmdIndex();
        }
    },
    /**
     * Actions Enum value.
     */
    LOGIN {
        {
            command = new CmdLogin();
        }
    },
    /**
     * Actions Enum value.
     */
    EDITUSERS {
        {
            command = new CmdEditUsers();
        }
    },
    /**
     * Actions Enum value.
     */
    ADDBICYCLE {
        {
            command = new CmdAddBicycle();
        }
    },
    /**
     * Actions Enum value.
     */
    ADDRENTPLACE {
        {
            command = new CmdAddRentPlace();
        }
    },
    /**
     * Actions Enum value.
     */
    ADDTYPE {
        {
            command = new CmdAddType();
        }
    },
    /**
     * Actions Enum value.
     */
    EDITTYPES {
        {
            command = new CmdEditTypes();
        }
    },
    /**
     * Actions Enum value.
     */
    EDITRENTPLACES {
        {
            command = new CmdEditRentPlaces();
        }
    },
    /**
     * Actions Enum value.
     */
    EDITBICYCLES {
        {
            command = new CmdEditBicycles();
        }
    },
    /**
     * Actions Enum value.
     */
    ERROR,
    /**
     * Actions Enum value.
     */
    USERERROR,
    /**
     * Actions Enum value.
     */
    PROFILE {
        {
            command = new CmdProfile();
        }
    },
    /**
     * Actions Enum value.
     */
    CARD {
        {
            command = new CmdCard();
        }
    },
    /**
     * Actions Enum value.
     */
    PREORDER {
        {
            command = new CmdPreOrder();
        }
    },
    /**
     * Actions Enum value.
     */
    CREATEORDER {
        {
            command = new CmdCreateOrder();
        }
    },
    /**
     * Actions Enum value.
     */
    VIEWUSERORDERS {
        {
            command = new CmdViewUserOrders();
        }
    },
    /**
     * Actions Enum value.
     */
    SUMMARY {
        {
            command = new CmdSummary();
        }
    },
    /**
     * Actions Enum value.
     */
    SIGNUP {
        {
            command = new CmdSignUp();
        }
    };
    /**
     * Public field to provide Command class for execution.
     */
    public Command command;
    /**
     * Public field to provide JSP page name for forward/redirect action.
     */
    public String jsp = "/jsp/" + this.toString().toLowerCase() + ".jsp";
}
