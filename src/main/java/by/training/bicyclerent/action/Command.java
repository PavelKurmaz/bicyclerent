package by.training.bicyclerent.action;

import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.exception.UserException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Abstract class for further Command
 * implementations.
 * @author Pavel Kurmaz.
 * @version 1.0
 */
public abstract class Command {

    /**
     * Abstract method that executes Get method for concrete AbstractCommand.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws AppException - Application exception thrown by method.
     * @throws UserException - User Interface exception thrown by method.
     * @return - String value to be processed by Servlet.
     */
    public abstract String executeGet(HttpServletRequest request,
                                   HttpServletResponse response)
            throws AppException, UserException;
    /**
     * Abstract method that executes Post method for concrete AbstractCommand.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws AppException - Application exception thrown by method.
     * @throws UserException - User Interface exception thrown by method.
     * @return - String value to be processed by Servlet.
     */
    public abstract String executePost(HttpServletRequest request,
                                       HttpServletResponse response)
            throws AppException, UserException;
}
