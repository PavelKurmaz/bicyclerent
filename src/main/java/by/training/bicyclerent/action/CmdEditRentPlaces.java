package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.entity.RentPlace;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.Paginator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

/**
 * Command class to implement Application logic.
 * Allows editing of all RentPlace Entities.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdEditRentPlaces extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<RentPlace> dao = new RentPlaceDao(connection);
        List<RentPlace> places = dao.getAll();
        int pageNumber = Paginator.getPageNumber(request, places.size());
        int startPosition = Paginator.getStartPosition(pageNumber);
        int finishPosition = Paginator.getFinishPosition(places.size(),
                pageNumber + 1);
        places = places.subList(startPosition, finishPosition);
        request.setAttribute("places", places);
        ConnectionPool.getInstance().freeConnection(connection);
        return "editrentplaces";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        RentPlaceDao dao = new RentPlaceDao(connection);
        String district = Validator.getString(request, "district");
        if (request.getParameter("Add") != null) {
            if (dao.readByName(district).getDistrict() != null) {
                throw new AppException("Rentplace in district "
                        + district + " already exists!");
            }
            RentPlace rentPlace = new RentPlace();
            rentPlace.setDistrict(district);
            int id = dao.create(rentPlace);
            if (id != -1) {
                rentPlace.setId(id);
            } else {
                throw new AppException("Failed to create Rent Place!");
            }
        } else if (request.getParameter("Update") != null) {
            RentPlace rentPlace = dao.read(Validator.getInteger(request, "id"));
            rentPlace.setDistrict(district);
            if (!dao.update(rentPlace)) {
                throw new AppException("Failed to update Rent Place!");
            }
        } else if (request.getParameter("Delete") != null) {
            RentPlace rentPlace = dao.read(Validator.getInteger(request, "id"));
            if (!dao.delete(rentPlace)) {
                throw new AppException("Failed to delete Rent Place!");
            }
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return null;
    }
}
