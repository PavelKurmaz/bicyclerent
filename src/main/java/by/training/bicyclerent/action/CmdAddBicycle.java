package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.RentPlace;
import by.training.bicyclerent.entity.Type;
import by.training.bicyclerent.exception.AppException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Command class to implement Application logic.
 * Creates and adds new Bicycle Entity.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdAddBicycle extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<Type> typeDao = new TypeDao(connection);
        Dao<RentPlace> rentPlaceDao = new RentPlaceDao(connection);
        List<Type> types;
        List<RentPlace> places;
        try {
            connection.setAutoCommit(false);
            types = typeDao.getAll();
            places = rentPlaceDao.getAll();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new AppException(e.getMessage());
        }
        request.setAttribute("types", types);
        request.setAttribute("places", places);
        ConnectionPool.getInstance().freeConnection(connection);
        return "addbicycle";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        return null;
    }
}
