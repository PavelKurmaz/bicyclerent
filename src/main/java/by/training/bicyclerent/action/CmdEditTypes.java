package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.Type;
import by.training.bicyclerent.exception.AppException;
import by.training.bicyclerent.util.Generator;
import by.training.bicyclerent.util.Paginator;
import by.training.bicyclerent.util.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.List;
import java.util.Random;

/**
 * Command class to implement Application logic.
 * Allows editing of all Type Entities.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdEditTypes extends Command {
    /**
     * Get method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<Type> dao = new TypeDao(connection);
        List<Type> types = dao.getAll();
        int pageNumber = Paginator.getPageNumber(request, types.size());
        int startPosition = Paginator.getStartPosition(pageNumber);
        int finishPosition = Paginator.getFinishPosition(types.size(),
                pageNumber + 1);
        types = types.subList(startPosition, finishPosition);
        request.setAttribute("types", types);
        ConnectionPool.getInstance().freeConnection(connection);
        return "edittypes";
    }

    /**
     * Post method processing for concrete Command.
     *
     * @param request  - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     * @throws AppException - Application exception thrown by method.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response)
            throws AppException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        Dao<Type> dao = new TypeDao(connection);
        String description = Validator.getString(request, "description");
        if (request.getParameter("Add") != null) {
            Type type = new Type();
            type.setDescription(description);
            try (InputStream content = request.getPart("image")
                    .getInputStream()) {
                if (content.available() != 0) {
                    String filename = Generator.generateRandomInt(1000)
                            + request.getPart("image").getSubmittedFileName();
                    String imagesPath = request.getServletContext()
                            .getInitParameter("imagespath");
                    File file = new File(imagesPath + File.separator + filename);
                    file.getParentFile().mkdirs();
                    Path path = file.toPath();
                    while (Files.isReadable(path)) {
                        file = new File(imagesPath
                                + Generator.generateRandomInt(10)
                                + filename);
                        path = file.toPath();
                    }
                    Files.copy(content, path);
                    type.setPath("img/" + filename);
                } else {
                    type.setPath("img/random.png");
                }
            } catch (IOException | ServletException e) {
                throw new AppException("Error reading image!");
            }
            int id = dao.create(type);
            if (id != -1) {
                type.setId(id);
            } else {
                throw new AppException("Failed to create Bicycle Type!");
            }
        } else if (request.getParameter("Update") != null) {
            Type type = dao.read(Validator.getInteger(request, "id"));
            type.setDescription(description);
            if (!dao.update(type)) {
                throw new AppException("Failed to update Bicycle Type!");
            }
        } else if (request.getParameter("Delete") != null) {
            Type type = dao.read(Validator.getInteger(request, "id"));
            if (!dao.delete(type)) {
                throw new AppException("Failed to delete Bicycle Type!");
            }
        }
        ConnectionPool.getInstance().freeConnection(connection);
        return null;
    }
}
