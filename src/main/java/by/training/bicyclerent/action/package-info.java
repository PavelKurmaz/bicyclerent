/**
 * Package contains Action and Command classes for BicycleRent Project.
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.action;
