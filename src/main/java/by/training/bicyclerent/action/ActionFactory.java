package by.training.bicyclerent.action;

import by.training.bicyclerent.exception.AppException;

import javax.servlet.http.HttpServletRequest;

/**
 * Factory Class to generate Actions Enum classes.
 *
 * @author Pavel Kurmaz.
 * @version 1.0
 */
public class ActionFactory {

    /**
     * Method to parse and generate Action class.
     *
     * @param request - HttpServletRequest object.
     * @return - AbstractCommand class generated.
     * @throws AppException - Application exception thrown by method.
     */
    public Actions getAction(final HttpServletRequest request)
            throws AppException {
        final String commandToParse = request.getParameter("command");
        if (commandToParse != null && !commandToParse.isEmpty()) {
            return Actions.valueOf(commandToParse.toUpperCase());
        } else {
            throw new AppException("No such command available");
        }
    }
}
