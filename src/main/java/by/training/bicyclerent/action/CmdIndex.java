package by.training.bicyclerent.action;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.Dao;
import by.training.bicyclerent.dao.ProfileDao;
import by.training.bicyclerent.entity.Profile;
import by.training.bicyclerent.entity.User;
import by.training.bicyclerent.util.Generator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.Locale;
/**
 * Command class to implement Application logic.
 * Realizes all main page functions.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CmdIndex extends Command {
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     */
    @Override
    public String executeGet(final HttpServletRequest request,
                             final HttpServletResponse response) {
        if (request.getParameter("locale") != null) {
            final String localeType = request.getParameter("locale");
            Locale locale = Generator.setLocale(localeType);
            HttpSession session = request.getSession();
            session.setAttribute("lang", locale);
            return null;
        }
        User user = (User) request.getSession().getAttribute("user");
        if (user != null) {
            Connection connection = ConnectionPool.getInstance()
                    .getConnection();
            Dao<Profile> dao = new ProfileDao(connection);
            Profile profile = dao.read(user.getId());
            request.getSession().setAttribute("profile", profile);
            ConnectionPool.getInstance().freeConnection(connection);
            return "index";
        } else {
            return "login";
        }

    }
    /**
     * Get method processing for concrete Command.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @return - String value of page name for forward/redirect by servlet.
     */
    @Override
    public String executePost(final HttpServletRequest request,
                              final HttpServletResponse response) {
        if (request.getParameter("lang") != null) {
            final String localeType = request.getParameter("lang");
            Locale locale = Generator.setLocale(localeType);
            HttpSession session = request.getSession();
            session.setAttribute("lang", locale);
        }
        return "login";
    }
}

