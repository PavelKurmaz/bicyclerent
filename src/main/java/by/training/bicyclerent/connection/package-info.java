/**
 * Package contains ConnectionPool class for BicycleRent Project.
 * Class used for mysql database connection provision.
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.connection;
