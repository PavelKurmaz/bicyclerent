package by.training.bicyclerent.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class to create and control database Connection Pool.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class ConnectionPool {
    /**
     * Object class Singleton instance.
     */
    private static ConnectionPool instance = new ConnectionPool();
    /**
     * Queue field to hold available Connections.
     */
    private BlockingQueue<Connection> connections = new LinkedBlockingQueue<>();
    /**
     * Queue field to hold occupied Connections.
     */
    private BlockingQueue<Connection> usedConnections
            = new LinkedBlockingQueue<>();
    /**
     * String value for Username database connection.
     */
    private static String username;
    /**
     * String value for Password database connection.
     */
    private static String password;
    /**
     * String value for url database connection.
     */
    private static String url;
    /**
     * Log4j Info level Logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /*
     * Static block to read and fill field values.
     */
    static {
        ResourceBundle bundle = ResourceBundle.getBundle("configuration");
        String driver = bundle.getString("database.driver");
        username = bundle.getString("database.username");
        password = bundle.getString("database.password");
        url = bundle.getString("database.url");
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * Getter method to retrieve available Connection.
     *
     * @return - retrieved Connection class object.
     */
    public Connection getConnection() {
        Connection connection = null;
        while (connection == null) {
            try {
                if (connections.isEmpty()) {
                    connection = DriverManager.getConnection(
                            url, username, password);
                    usedConnections.put(connection);
                } else {
                    connection = connections.take();
                    usedConnections.put(connection);
                }
            } catch (SQLException | InterruptedException e) {
                LOG.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
        return connection;
    }

    /**
     * Method used to return unnecessary Connection object back to pool.
     *
     * @param connection - Connection class object to return to pool.
     */
    public boolean freeConnection(final Connection connection)  {
        if (usedConnections.remove(connection)) {
            try {
                connections.put(connection);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Getter method for Singleton instance.
     *
     * @return - ConnectionPool instance to return.
     */

    public static ConnectionPool getInstance() {
        return instance;
    }

    public void closeConnections() {
        for (Connection connection: usedConnections) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOG.error(e.getMessage());
            }
        }
        for (Connection connection: connections) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOG.error(e.getMessage());
            }
        }
    }
}


