package by.training.bicyclerent.util;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

/**
 * Generator class to create entities and
 * database user for Servlet.
 *
 * @author Pavel Kurmaz
 * @version 1.0
 */

public final class Generator {
    /**
     * Random class object for data generation.
     */
    private static Random random = new Random();

    private static final String CREATE_DB_USER = "GRANT"
            + " CREATE, CREATE VIEW, INSERT, SELECT, UPDATE, DELETE ON *.*"
            + " TO 'appuser'@'localhost' IDENTIFIED BY '321';\n"
            + " FLUSH PRIVILEGES;";

    /**
     * Private class constructor.
     */
    private Generator() {
    }

    /**
     * Log4j INFO level Logger.
     */
    public static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Method to generate administrator entity and store it in database.
     *
     * @param login    - administrator login;
     * @param password - administrator password;
     * @param email    - administrator e-mail;
     * @return - boolean value to signal creation success.
     */
    public static boolean generateAdmin(final String login,
                                        final String password,
                                        final String email) {
        Connection connection = ConnectionPool.getInstance().getConnection();
        UserDao dao = new UserDao(connection);
        if (dao.readByName(login).getStatus() == null) {
            User user = new User();
            user.setLogin(login);
            user.setPassword(password);
            user.setRole(1);
            user.setStatus(Status.GOLD);
            user.setEmail(email);
            user.setBlocked(false);
            user.setPath("img/avatar/random.jpg");
            return dao.create(user) != 0;
        }
        return true;
    }

    /**
     * Method to generate user for database connection.
     *
     * @param username - database user name;
     * @param password - database user password;
     * @param path     - path to properties file to store database connection data.
     */
    public static void generateDbAdmin(final String username,
                                       final String password,
                                       final String path) {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.execute(CREATE_DB_USER);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        try (InputStream in = Files.newInputStream(Paths.get(path))) {
            Properties properties = new Properties();
            properties.load(in);
            properties.setProperty("database.username", username);
            properties.setProperty("database.password", password);
            OutputStream out = Files.newOutputStream(Paths.get(path));
            properties.store(out, null);
            out.close();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * Static method for int values generation.
     *
     * @param bound - int value for generation limit.
     * @return - generated int value.
     */
    public static int generateRandomInt(int bound) {
        return random.nextInt(bound);
    }

    public static Locale setLocale(String localeType) {
        Locale locale;
        switch (localeType) {
            case "US":
                locale = new Locale("en", "US");
                break;
            case "RU":
                locale = new Locale("ru", "RU");
                break;
            case "BY":
                locale = new Locale("be", "BY");
                break;
            default:
                locale = new Locale("en", "US");
                break;
        }
        return locale;
    }
}
