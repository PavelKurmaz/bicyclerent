package by.training.bicyclerent.util;

import by.training.bicyclerent.entity.CreditCard;
import by.training.bicyclerent.entity.Order;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Utilitary class for Order payment processes.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CashUtils {
    /**
     * Private static constant that holds default value for maximum overdraft.
     */
    private static final BigDecimal MAX_OVERDRAFT = BigDecimal.valueOf(100);
    /**
     * Private class constructor.
     */
    private CashUtils() { }

    /**
     * Utilitary method for altering user`s CreditCard balance.
     *
     * @param creditCard - user`s CreditCard Entity to process.
     * @param cost       - Order`s cost to withdraw.
     */
    public static void proceedPayment(final CreditCard creditCard,
                                final BigDecimal cost) {
        BigDecimal balance = creditCard.getBalance();
        creditCard.setBalance(balance.subtract(cost));
        if (creditCard.getBalance().doubleValue() < 0) {
            creditCard.setOverdraft(creditCard.getOverdraft()
                    .add(creditCard.getBalance().abs()));
            creditCard.setBalance(BigDecimal.ZERO);
        }
    }

    /**
     * Utilitary method for balance check procedure.
     *
     * @param creditCard - CreditCard Entity to check balance.
     * @param cost       - estimated cost for new Order.
     * @return - boolean value signals if User has enough funds for Order.
     */
    public static boolean checkBalance(final CreditCard creditCard,
                                 final BigDecimal cost) {
        BigDecimal balance = creditCard.getBalance();
        if (balance.doubleValue() == 0) {
            BigDecimal overdraft = creditCard.getOverdraft();
            if (cost.doubleValue() > (MAX_OVERDRAFT.doubleValue()
                    - overdraft.doubleValue())) {
                return false;
            }
        }
        return true;
    }
    public static boolean upgradeUser(User user, List<Order> orders) {
        if (orders.size() > 5 && orders.size() < 10) {
            user.setStatus(Status.SILVER);
            return true;
        } else if ( orders.size() > 10) {
            user.setStatus(Status.GOLD);
            return true;
        }
        return false;
    }
}
