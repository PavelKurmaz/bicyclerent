package by.training.bicyclerent.util;

import by.training.bicyclerent.exception.AppException;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Utility class for input data validation.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public final class Validator {
    /**
     * Private class constructor.
     */
    private Validator() { }
    /**
     * Pattern for String data validation.
     */
    private static final String STRING = "[\\+A-za-z0-9\\s-_]+";
    /**
     * Pattern for Integer data validation.
     */
    private static final String INTEGER = "-?[0-9]+";
    /**
     * Pattern for Double data validation.
     */
    private static final String DOUBLE = "-?[0-9]+\\.?([0-9]*)";
    /**
     * Pattern for e-mail data validation.
     */
    private static final String EMAIL =
            "[0-9A-Za-z_-]{0,20}@[0-9A-Za-z]{0,12}\\.[a-z]{2,4}";

    /**
     * Utility Method for String data validation.
     * @param req - HttpServletRequest object.
     * @param field - String type input data to validate.
     * @param pattern - Pattern to check input data.
     * @return - Validated String data.
     * @throws AppException - exception thrown in case of failed validation.
     */
    private static String getString(final HttpServletRequest req,
                                    final String field,
                                    final String pattern) throws AppException {
        String value = req.getParameter(field);
        if (value == null) {
            throw new AppException("Data not found");
        }
        if (value.matches(pattern)) {
            return value;
        } else {
            throw new AppException("Wrong input data type");
        }
    }

    /**
     * Interface method for E-mail data validation.
     * @param req - HttpServletRequest object.
     * @return - validated String data.
     * @throws AppException - exception thrown in case of failed validation.
     */
    public static String getEmail(final HttpServletRequest req)
            throws AppException {
        return getString(req, "email", EMAIL);
    }

    /**
     * Interface method for String data validation.
     * @param req - HttpServletRequest object.
     * @param field - String data to validate.
     * @return - validated String data.
     * @throws AppException - exception thrown in case of failed validation.
     */
    public static String getString(final HttpServletRequest req,
                                   final String field) throws AppException {
        return getString(req, field, STRING);
    }
    /**
     * Interface method for Integer data validation.
     * @param req - HttpServletRequest object.
     * @param field - String data to validate.
     * @return - validated int data.
     * @throws AppException - exception thrown in case of failed validation.
     */
    public static Integer getInteger(final HttpServletRequest req,
                                 final String field) throws AppException {
        return Integer.valueOf(getString(req, field, INTEGER));
    }
    /**
     * Interface method for E-mail data validation.
     * @param field - String data to validate.
     * @param req - HttpServletRequest object.
     * @return - validated double data.
     * @throws AppException - exception thrown in case of failed validation.
     */
    public static BigDecimal getDouble(final HttpServletRequest req,
                                       final String field) throws AppException {
        String value = getString(req, field, DOUBLE);
        return BigDecimal.valueOf(Double.valueOf(value));
    }
}
