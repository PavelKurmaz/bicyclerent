package by.training.bicyclerent.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Paginator utility class for view`s page implementation.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public final class Paginator {
    /**
     * Private class constructor.
     */
    private Paginator() { }

    /**
     * Int constant to determine max number of values displayed on one page.
     */
    private static final int MAX_LINES_ON_PAGE = 5;

    /**
     * Method to determine index of last entity position to be displayed.
     * @param size - initial size of entity array;
     * @param pageNumber - number of displayed page;
     * @return - int value of last entity index.
     */
    public static int getFinishPosition(final int size,
                                        final int pageNumber) {
        if (size == 0) {
            return size;
        } else if (size / (pageNumber * MAX_LINES_ON_PAGE) > 0) {
            return pageNumber * (MAX_LINES_ON_PAGE);
        } else {
            return size;
        }
    }

    /**
     * Method to determine index of first entity position to be displayed.
     * @param pageNumber - number of displayed page;
     * @return - int value of first entity index;
     */
    public static int getStartPosition(final int pageNumber) {
        return pageNumber * MAX_LINES_ON_PAGE;
    }

    /**
     * Method to determine number of requested page.
     * @param request - HttpServletRequest object.
     * @param size - initial size of entity array;
     * @return - int value of displayed page number;
     */
    public static int getPageNumber(final HttpServletRequest request,
                                    final int size) {
        int page = 1;
        int maxPages = (size - 1) / MAX_LINES_ON_PAGE + 1;
        request.setAttribute("maxpages", maxPages);
        if (request.getParameter("Previous") != null) {
            page = (int) request.getSession()
                    .getAttribute("pagenumber") - 1;
            request.getSession().setAttribute("pagenumber", page);
        } else if (request.getParameter("Next") != null) {
            page = (int) request.getSession()
                    .getAttribute("pagenumber") + 1;
            request.getSession().setAttribute("pagenumber", page);
        } else {
            request.getSession().setAttribute("pagenumber", 1);
        }
        return page - 1;
    }
}
