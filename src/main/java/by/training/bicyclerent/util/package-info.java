/**
 * Package contains Utility classes for BicycleRent Project.
 * @author Pavel Kurmaz
 * @version 1.0
 */
package by.training.bicyclerent.util;
