package bicyclerent;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.Type;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Test class for Type Data access objects.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class TypeDaoTest {
    private static Type type;
    private static TypeDao dao;

    @BeforeClass
    public static void init()  {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        type = new Type();
        type.setPath("test");
        type.setDescription("test");
        dao = new TypeDao(connectionPool.getConnection());
    }

    @Test
    public void createType() {
        int id = dao.readByDescription("test").getId();
        if (id != 0) {
            type.setId(id);
            dao.delete(type);
        }
        id = dao.create(type);
        type.setId(id);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readType() {
        Type testType = dao.read(type.getId());
        Assert.assertTrue(type.equals(testType));
    }

    @Test
    public void readTypeByDescription() {
        Type testType = dao.readByDescription(type.getDescription());
        Assert.assertTrue(type.equals(testType));
    }

    @Test
    public void updateType() {
        type.setDescription("newDescription");
        Assert.assertTrue(dao.update(type));
    }

    @Test
    public void readAllTypes() {
        List<Type> types = dao.getAll();
        Assert.assertFalse(types.isEmpty());
    }

    @AfterTest
    public void deleteType() {
        Assert.assertTrue(dao.delete(type));
    }
}
