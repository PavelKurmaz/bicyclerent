package bicyclerent;


import by.training.bicyclerent.connection.ConnectionPool;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;

/**
 * Test class for Database connection.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class ConnectionTest {

    private static ConnectionPool connectionPool;

    @BeforeClass
    public static void init()  {
        connectionPool = ConnectionPool.getInstance();
    }

    @Test
    public void getConnection() {
        Connection connection = connectionPool.getConnection();
        Assert.assertTrue(connection != null);
    }

    @Test
    public void returnConnection() {
        Connection connection = connectionPool.getConnection();
        Assert.assertTrue(connectionPool.freeConnection(connection));
    }
}
