package bicyclerent;


import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Status;
import by.training.bicyclerent.entity.User;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Test class for User Data access objects.
 * @author Pavel Kurmaz
 * @version 1.0
 */

public class UserDaoTest {
    private static User user;
    private static UserDao dao;

    @BeforeClass
    public static void init()  {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        user = new User();
        user.setPath("test");
        user.setPassword("test");
        user.setDisabled(false);
        user.setBlocked(false);
        user.setStatus(Status.GOLD);
        user.setLogin("test");
        user.setEmail("test@test.by");
        user.setRole(0);
        dao = new UserDao(connectionPool.getConnection());
    }

    @Test
    public void createUser() {
        int id = dao.readByName("test").getId();
        if (id != 0) {
            user.setId(id);
            dao.delete(user);
        }
        id = dao.create(user);
        user.setId(id);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readUser() {
        User testUser = dao.read(user.getId());
        Assert.assertTrue(user.equals(testUser));
    }

    @Test
    public void readUserByName() {
        User testUser = dao.readByName(user.getLogin());
        Assert.assertTrue(user.equals(testUser));
    }

    @Test
    public void updateUser() {
        user.setRole(1);
        Assert.assertTrue(dao.update(user));
    }

    @Test
    public void readAllUsers() {
        List<User> users = dao.getAll();
        Assert.assertFalse(users.isEmpty());
    }

    @AfterTest
    public void deleteUser() {
        Assert.assertTrue(dao.delete(user));
    }
}
