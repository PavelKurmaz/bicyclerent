package bicyclerent;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.dao.TypeDao;
import by.training.bicyclerent.entity.Bicycle;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

/**
 * Test class for Bicycle Data access objects. 
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class BicycleDaoTest {
    private static Bicycle bicycle;
    private static BicycleDao dao;

    @BeforeClass
    public static void init()  {
        Connection connection = ConnectionPool.getInstance().getConnection();
        dao = new BicycleDao(connection);
        TypeDao typeDao = new TypeDao(connection);
        RentPlaceDao rentPlaceDao = new RentPlaceDao(connection);
        bicycle = new Bicycle();
        bicycle.setRate(BigDecimal.valueOf(0.0));
        bicycle.setRentPlaceId(rentPlaceDao.getAll().get(0).getId());
        bicycle.setTypeId(typeDao.getAll().get(0).getId());
        bicycle.setModel("test");
    }

    @Test
    public void createBicycle() {
        int id = dao.create(bicycle);
        bicycle.setId(id);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readBicycle() {
        Bicycle testBicycle = dao.read(bicycle.getId());
        Assert.assertTrue(bicycle.equals(testBicycle));
    }

    @Test
    public void updateBicycle() {
        bicycle.setModel("newModel");
        Assert.assertTrue(dao.update(bicycle));
    }

    @Test
    public void readAllBicycles() {
        List<Bicycle> bicycles = dao.getAll();
        Assert.assertFalse(bicycles.isEmpty());
    }

    @Test
    public void readByTypeAndPlace() {
        List<Bicycle> bicycles = dao.selectByTypeAndPlace(bicycle.getTypeId(), bicycle.getRentPlaceId());
        Assert.assertFalse(bicycles.isEmpty());
    }

    @AfterTest
    public void deleteBicycle() {
        Assert.assertTrue(dao.delete(bicycle));
    }
}
