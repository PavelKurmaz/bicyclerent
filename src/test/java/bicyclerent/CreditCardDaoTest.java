package bicyclerent;


import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.CreditCardDao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.CreditCard;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

/**
 * Test class for CreditCard Data access objects.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class CreditCardDaoTest {
    private static CreditCard creditCard;
    private static CreditCardDao dao;

    @BeforeClass
    public static void init()  {
        Connection connection = ConnectionPool.getInstance().getConnection();
        dao = new CreditCardDao(connection);
        creditCard = new CreditCard();
        creditCard.setCardNumber("1111222233334444");
        creditCard.setBalance(BigDecimal.valueOf(0.0));
        creditCard.setOverdraft(BigDecimal.valueOf(0.0));
        UserDao userDao = new UserDao(connection);
        int id = userDao.getAll().get(0).getId();
        creditCard.setUserId(id);
    }

    @Test
    public void createCreditCard() {
        int id = dao.create(creditCard);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readCreditCard() {
        CreditCard testCreditCard = dao.read(creditCard.getUserId());
        Assert.assertTrue(creditCard.equals(testCreditCard));
    }

    @Test
    public void updateCreditCard() {
        creditCard.setBalance(BigDecimal.ONE);
        Assert.assertTrue(dao.update(creditCard));
    }

    @Test
    public void readAllCreditCards() {
        List<CreditCard> creditCards = dao.getAll();
        Assert.assertFalse(creditCards.isEmpty());
    }

    @AfterTest
    public void deleteCreditCard() {
        Assert.assertTrue(dao.delete(creditCard));
    }
}
