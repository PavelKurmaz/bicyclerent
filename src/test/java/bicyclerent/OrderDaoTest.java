package bicyclerent;


import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.BicycleDao;
import by.training.bicyclerent.dao.OrderDao;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Order;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;


public class OrderDaoTest {
    private static Order order;
    private static OrderDao dao;

    @BeforeClass
    public static void init()  {
        Connection connection = ConnectionPool.getInstance().getConnection();
        dao = new OrderDao(connection);
        BicycleDao bicycleDao = new BicycleDao(connection);
        UserDao userDao = new UserDao(connection);
        RentPlaceDao rentPlaceDao = new RentPlaceDao(connection);
        order = new Order();
        order.setStatus(false);
        order.setEstimate(1);
        order.setDate(new Date(System.currentTimeMillis()));
        order.setBicycleId(bicycleDao.getAll().get(0).getId());
        order.setUserId(userDao.getAll().get(0).getId());
        order.setStart(rentPlaceDao.getAll().get(0).getId());
        order.setFinish(rentPlaceDao.getAll().get(0).getId());
    }

    @Test
    public void createOrder() {
        int id = dao.create(order);
        order.setId(id);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readOrder() {
        Order testOrder = dao.read(order.getId());
        Assert.assertTrue(order.equals(testOrder));
    }

    @Test
    public void readOrderByUserId() {
        int userId = order.getUserId();
        List<Order> orders = dao.getByUserId(userId);
        Assert.assertFalse(orders.isEmpty());
    }

    @Test
    public void readOrderByPlaceId() {
        int placeId = order.getStart();
        List<Order> orders = dao.getByPlaceId(placeId);
        Assert.assertFalse(orders.isEmpty());
    }

    @Test
    public void updateOrder() {
        order.setStart(1);
        Assert.assertTrue(dao.update(order));
    }

    @Test
    public void readAllOrders() {
        List<Order> orders = dao.getAll();
        Assert.assertFalse(orders.isEmpty());
    }

    @AfterTest
    public void deleteOrder() {
        Assert.assertTrue(dao.delete(order));
    }
}
