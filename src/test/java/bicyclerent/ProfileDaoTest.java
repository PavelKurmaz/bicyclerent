package bicyclerent;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.ProfileDao;
import by.training.bicyclerent.dao.UserDao;
import by.training.bicyclerent.entity.Profile;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/** 
 * Test class for Profile Data access objects.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class ProfileDaoTest {
    private static Profile profile;
    private static ProfileDao dao;

    @BeforeClass
    public static void init()  {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        UserDao userDao = new UserDao(connectionPool.getConnection());
        int userId = userDao.getAll().get(0).getId();
        profile = new Profile();
        profile.setUserId(userId);
        profile.setPhone("test");
        profile.setFirstName("test");
        profile.setLastName("test");
        profile.setAddress("test");
        dao = new ProfileDao(connectionPool.getConnection());
    }

    @Test
    public void createProfile() {
        dao.delete(profile);
        int id = dao.create(profile);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readProfile() {
        Profile testProfile = dao.read(profile.getUserId());
        Assert.assertTrue(profile.equals(testProfile));
    }

    @Test
    public void updateProfile() {
        profile.setAddress("newAddress");
        Assert.assertTrue(dao.update(profile));
    }

    @Test
    public void readAllProfiles() {
        List<Profile> profiles = dao.getAll();
        Assert.assertFalse(profiles.isEmpty());
    }

    @AfterTest
    public void deleteProfile() {
        Assert.assertTrue(dao.delete(profile));
    }
}
