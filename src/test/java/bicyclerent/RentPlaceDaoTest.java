package bicyclerent;

import by.training.bicyclerent.connection.ConnectionPool;
import by.training.bicyclerent.dao.RentPlaceDao;
import by.training.bicyclerent.entity.RentPlace;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Test class for RentPlace Data access objects.
 * @author Pavel Kurmaz
 * @version 1.0
 */
public class RentPlaceDaoTest {
    private static RentPlace rentPlace;
    private static RentPlaceDao dao;

    @BeforeClass
    public static void init()  {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        rentPlace = new RentPlace();
        rentPlace.setDistrict("test");
        dao = new RentPlaceDao(connectionPool.getConnection());
    }

    @Test
    public void createRentPlace() {
        int id = dao.readByName("test").getId();
        if (id != 0) {
            rentPlace.setId(id);
            dao.delete(rentPlace);
        }
        id = dao.create(rentPlace);
        rentPlace.setId(id);
        Assert.assertTrue(id != -1);
    }

    @Test
    public void readRentPlace() {
        RentPlace testRentPlace = dao.read(rentPlace.getId());
        Assert.assertTrue(rentPlace.equals(testRentPlace));
    }

    @Test
    public void readRentPlaceByName() {
        RentPlace testRentPlace = dao.readByName(rentPlace.getDistrict());
        Assert.assertTrue(rentPlace.equals(testRentPlace));
    }

    @Test
    public void updateRentPlace() {
        rentPlace.setDistrict("newDistrict");
        Assert.assertTrue(dao.update(rentPlace));
    }

    @Test
    public void readAllRentPlaces() {
        List<RentPlace> rentPlaces = dao.getAll();
        Assert.assertFalse(rentPlaces.isEmpty());
    }

    @AfterTest
    public void deleteRentPlace() {
        Assert.assertTrue(dao.delete(rentPlace));
    }
}
